# Set common home path
ARG BUILD_HOME=/owl2dl-ccc

# Build

## Base image
FROM gradle:8.0.2-jdk17-alpine as build-image

## Set up working directory
ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
WORKDIR $APP_HOME

## Copy source and settings
COPY --chown=gradle:gradle build.gradle settings.gradle $APP_HOME/
COPY --chown=gradle:gradle src $APP_HOME/src

## Build the application
RUN gradle distTar --no-daemon

# Run
FROM eclipse-temurin:17

## Copy distribution tar and extract it
ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
COPY --from=build-image  $APP_HOME/build/distributions/owl2dl-ccc-0.0.9.tar owl2dl-ccc-0.0.9.tar

RUN mkdir /app
RUN tar -xf owl2dl-ccc-0.0.9.tar -C /app

WORKDIR /app/owl2dl-ccc-0.0.9/bin

ENTRYPOINT ["/app/owl2dl-ccc-0.0.9/bin/owl2dl-ccc"]
