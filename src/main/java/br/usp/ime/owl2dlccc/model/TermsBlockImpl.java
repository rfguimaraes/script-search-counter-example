/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import java.util.List;
import java.util.Optional;

public class TermsBlockImpl implements TermsBlock {

  protected List<PrimitiveTerm> primitives;
  protected List<CompositeTerm> composites;

  @Override
  public Optional<List<PrimitiveTerm>> getPrimitives() {
    return Optional.ofNullable(primitives);
  }

  @Override
  public void setPrimitives(List<PrimitiveTerm> primitives) {
    this.primitives = primitives;
  }

  @Override
  public Optional<List<CompositeTerm>> getComposites() {
    return Optional.ofNullable(composites);
  }

  @Override
  public void setComposites(List<CompositeTerm> composites) {
    this.composites = composites;
  }
}
