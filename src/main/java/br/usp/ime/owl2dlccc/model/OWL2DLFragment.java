/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.profiles.OWL2DLProfile;
import org.semanticweb.owlapi.profiles.OWL2ELProfile;
import org.semanticweb.owlapi.profiles.OWL2QLProfile;
import org.semanticweb.owlapi.profiles.OWL2RLProfile;
import org.semanticweb.owlapi.profiles.OWLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;

public enum OWL2DLFragment {
  EL,
  QL,
  RL,
  DL;

  private final static Map<OWL2DLFragment, OWLProfile> profileMap = ImmutableMap.<OWL2DLFragment, OWLProfile>builder()
      .put(EL, new OWL2ELProfile())
      .put(QL, new OWL2QLProfile())
      .put(RL, new OWL2RLProfile())
      .put(DL, new OWL2DLProfile())
      .build();

  public OWLProfile getProfile() {
    return profileMap.get(this);
  }

  public OWLProfileReport check(OWLOntology ontology) {
    return profileMap.get(this).checkOntology(ontology);
  }
}
