/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.semanticweb.owlapi.model.EntityType;

public class CompositeTermImpl extends TermImpl implements
    CompositeTerm {

  protected OWL2DLConstructor constructor;
  protected List<String> operands;

  public CompositeTermImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "entity_type", required = true) EntityType entityType) {
    super(name, entityType);
  }

  @JsonCreator
  public CompositeTermImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "entity_type", required = true) EntityType entityType,
      @JsonProperty(value = "constructor", required = true) OWL2DLConstructor constructor,
      @JsonProperty(value = "operands") List<String> operands) {
    super(name, entityType);
    this.constructor = constructor;
    this.operands = operands;
  }

  @Override
  public OWL2DLConstructor getConstructor() {
    return constructor;
  }

  @Override
  public void setConstructor(OWL2DLConstructor constructor) {
    this.constructor = constructor;
  }

  @Override
  public List<String> getOperands() {
    return operands;
  }

  @Override
  public void setOperands(List<String> operands) {
    this.operands = operands;
  }

  public int compareTo(Term other) {
    return this.name.compareTo(other.getName());
  }
}
