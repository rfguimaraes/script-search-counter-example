/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;

public class HeaderBlockImpl implements HeaderBlock {

  protected String name;
  protected String author;
  protected OWL2DLFragment inputFragment;
  protected OWL2DLFragment outputFragment;
  protected String formatVersion;
  protected String description;

  @JsonCreator
  public HeaderBlockImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "author") String author,
      @JsonProperty(value = "input_fragment") OWL2DLFragment inputFragment,
      @JsonProperty(value = "output_fragment") OWL2DLFragment outputFragment,
      @JsonProperty(value = "format_version") String formatVersion,
      @JsonProperty(value = "description") String description) {
    this.name = name;
    this.author = author;
    this.inputFragment = inputFragment;
    this.outputFragment = outputFragment;
    this.formatVersion = formatVersion;
    this.description = description;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Optional<String> getAuthor() {
    return Optional.ofNullable(author);
  }

  @Override
  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public Optional<OWL2DLFragment> getInputFragment() {
    return Optional.ofNullable(inputFragment);
  }

  @Override
  public void setInputFragment(OWL2DLFragment inputFragment) {
    this.inputFragment = inputFragment;
  }

  @Override
  public Optional<OWL2DLFragment> getOutputFragment() {
    return Optional.ofNullable(outputFragment);
  }

  @Override
  public void setOutputFragment(OWL2DLFragment outputFragment) {
    this.outputFragment = outputFragment;
  }

  @Override
  public Optional<String> getFormatVersion() {
    return Optional.ofNullable(formatVersion);
  }

  @Override
  public void setFormatVersion(String version) {
    this.formatVersion = version;
  }

  @Override
  public Optional<String> getDescription() {
    return Optional.ofNullable(description);
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }
}
