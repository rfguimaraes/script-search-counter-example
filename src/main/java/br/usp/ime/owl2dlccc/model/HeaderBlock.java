/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;

public interface HeaderBlock {

  @JsonProperty("name")
  String getName();

  @JsonProperty("name")
  void setName(String name);

  @JsonProperty("author")
  Optional<String> getAuthor();

  @JsonProperty("author")
  void setAuthor(String author);

  @JsonProperty("input_fragment")
  Optional<OWL2DLFragment> getInputFragment();

  @JsonProperty("input_fragment")
  void setInputFragment(OWL2DLFragment inputFragment);

  @JsonProperty("output_fragment")
  Optional<OWL2DLFragment> getOutputFragment();

  @JsonProperty("output_fragment")
  void setOutputFragment(OWL2DLFragment outputFragment);

  @JsonProperty("format_version")
  Optional<String> getFormatVersion();

  @JsonProperty("format_version")
  void setFormatVersion(String version);

  @JsonProperty("description")
  Optional<String> getDescription();

  @JsonProperty("description")
  void setDescription(String description);
}
