/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;

public interface ChangeSpecification {

  @JsonProperty("header")
  HeaderBlock getHeaderBlock();

  @JsonProperty("header")
  void setHeaderBlock(HeaderBlock headerBlock);

  @JsonProperty("terms")
  Optional<TermsBlock> getTermsBlock();

  @JsonProperty("terms")
  void setTermsBlock(TermsBlock termsBlock);

  @JsonProperty("preconditions")
  Optional<IntegrityConstraints> getPreconditions();

  @JsonProperty("preconditions")
  void setPreconditions(IntegrityConstraints preconditions);

  @JsonProperty("actions")
  Optional<ActionBlock> getActionBlock();

  @JsonProperty("actions")
  void setActionBlock(ActionBlock actionBlock);

  @JsonProperty("postconditions")
  Optional<IntegrityConstraints> getPostconditions();

  @JsonProperty("postconditions")
  void setPostconditions(IntegrityConstraints postconditions);

  @JsonProperty("catalysts")
  List<CatalystBlock> getCatalysts();

  @JsonProperty("catalysts")
  void setCatalysts(List<CatalystBlock> catalysts);
}
