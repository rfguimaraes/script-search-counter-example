/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import br.usp.ime.owl2dlccc.parsing.DefaultObjectMapper;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import org.semanticweb.owlapi.model.EntityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrimitiveTermImpl extends TermImpl implements
    PrimitiveTerm {

  private static final Logger logger = LoggerFactory.getLogger(PrimitiveTermImpl.class);

  protected Boolean used;
  protected Boolean complex;
  protected Boolean isOWLThing;
  protected Boolean isOWLNothing;

  public PrimitiveTermImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "entity_type", required = true) EntityType entityType) {
    super(name, entityType);
  }

  @JsonCreator
  public PrimitiveTermImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "entity_type", required = true) EntityType entityType,
      @JsonProperty(value = "used") Boolean used,
      @JsonProperty(value = "complex") Boolean complex,
      @JsonProperty(value = "owl_thing") Boolean isOWLThing,
      @JsonProperty(value = "owl_nothing") Boolean isOWLNothing) {
    super(name, entityType);
    this.used = used;
    this.complex = complex;
    this.isOWLThing = isOWLThing;
    this.isOWLNothing = isOWLNothing;
  }

  @Override
  public Optional<Boolean> getUsed() {
    return Optional.ofNullable(used);
  }

  @Override
  public void setUsed(Boolean used) {
    this.used = used;
  }

  @Override
  public Optional<Boolean> getComplex() {
    return Optional.ofNullable(complex);
  }

  @Override
  public void setComplex(Boolean complex) {
    this.complex = complex;
  }

  @Override
  public Optional<Boolean> getOWLThing() {
    return Optional.ofNullable(isOWLThing);
  }

  @Override
  public void setOWLThing(Boolean isOWLThing) {
    this.isOWLThing = isOWLThing;
  }

  @Override
  public Optional<Boolean> getOWLNothing() {
    return Optional.ofNullable(isOWLNothing);
  }

  @Override
  public void setOWLNothing(Boolean isOWLNothing) {
    this.isOWLNothing = isOWLNothing;
  }

  @Override
  public String toString() {
    try {
      return DefaultObjectMapper.getInstance().writeValueAsString(this);
    } catch (JsonProcessingException e) {
      logger.warn("Failure to convert {} directly as JSON string.", this);
      return super.toString();
    }
  }

  @Override
  public int compareTo(Term other) {
    return this.name.compareTo(other.getName());
  }

  @Override
  public List<String> getOperands() {
    return Lists.newArrayList(this.name);
  }
}
