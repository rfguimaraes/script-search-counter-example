/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CatalystBlockImpl implements CatalystBlock {

  String name;
  IntegrityConstraints constraints;

  @JsonCreator
  public CatalystBlockImpl(
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "constraints", required = true) IntegrityConstraints integrityConstraints) {
    this.name = name;
    this.constraints = integrityConstraints;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public IntegrityConstraints getConstraints() {
    return this.constraints;
  }

  @Override
  public void setConstraints(IntegrityConstraints constraints) {
    this.constraints = constraints;
  }
}
