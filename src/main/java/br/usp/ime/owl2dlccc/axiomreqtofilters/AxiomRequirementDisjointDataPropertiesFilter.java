/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.AsOWLDataProperty;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.HasDataPropertiesInSignature;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLDisjointDataPropertiesAxiomImpl;

public class AxiomRequirementDisjointDataPropertiesFilter extends
    AxiomRequirementNAryDataPropertyFilter<OWLDisjointDataPropertiesAxiom> {

public AxiomRequirementDisjointDataPropertiesFilter(
      OWLOntology ontology, OWLReasoner reasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLDisjointDataPropertiesAxiom buildAxiom(List<OWLDataPropertyExpression> properties) {
    return new OWLDisjointDataPropertiesAxiomImpl(properties, Collections.emptySet());
  }

  @Override
  public void dataPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLDataPropertyExpression> domain) {

    List<OWLDataPropertyExpression> properties = requirement.getAxiomSpecification().getArguments()
        .stream().map(match::getDataPropertyExpression)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());

    //P.S.: dataPropertiesInSignature works here only because currently DPE := DP on OWL 2
    List<OWLDataProperty> dataProperties = properties.stream().filter(AsOWLDataProperty::isOWLDataProperty).map(OWLDataPropertyExpression::asOWLDataProperty).collect(Collectors.toList());

    int totalProperties = requirement.getAxiomSpecification().getArguments().size();

    if (requirement.isAsserted().isPresent()) {
      if (requirement.isAsserted().get()) {
        if (dataProperties.isEmpty()) {
          // All undefined, but we can still look at those referenced by some axiom of that type
          /* P.S.: dataPropertiesInSignature works here only because currently DPE := DP on OWL 2
          specification */
          domain.retainAll(originalOntology.axioms(AxiomType.DISJOINT_DATA_PROPERTIES).flatMap(
              HasDataPropertiesInSignature::dataPropertiesInSignature).collect(
              Collectors.toSet()));
        } else {
          Set<OWLDataPropertyExpression> intersection = dataProperties.stream().skip(1)
              .map(pe -> EntitySearcher.getDisjointProperties(pe.asOWLDataProperty(), originalOntology).collect(
                  Collectors.toSet())).collect(() -> Sets.newHashSet(
                  EntitySearcher.getDisjointProperties(dataProperties.get(0), originalOntology).collect(
                      Collectors.toSet())), Set::retainAll, Set::retainAll);
          if (requirement.isAsserted().get()) {
            // At least one individual is defined and we want the axiom to be asserted
            domain.retainAll(intersection);
          } else if (properties.size() == totalProperties - 1) {
            // We can only exclude individuals that surely would make the axiom to be an asserted one,
            // hence from the n different individuals n - 1 must be defined already
            domain.removeAll(intersection);
          }
        }
      }
    }

    // For entailment we need at least one data property
    if (requirement.isEntailed().isPresent() && !properties.isEmpty()) {
      Set<OWLDataPropertyExpression> intersection = dataProperties.stream().skip(1)
          .map(pe -> extendedReasoner.disjointDataProperties(pe).collect(Collectors.toSet()))
          .collect(() -> Sets.newHashSet(
              extendedReasoner.disjointDataProperties(dataProperties.get(0)).collect(
                  Collectors.toSet())), Set::retainAll, Set::retainAll);

      if (requirement.isEntailed().get()) {
        domain.retainAll(intersection);
      } else if (properties.size() == totalProperties - 1) {
        // We can only really exclude if all but one individual (the pivot) are defined
        domain.removeAll(intersection);
      }
    }
  }
}
