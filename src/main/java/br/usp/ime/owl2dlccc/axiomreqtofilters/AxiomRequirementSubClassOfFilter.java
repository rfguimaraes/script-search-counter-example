/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;

public class AxiomRequirementSubClassOfFilter extends
    AxiomRequirementClassAxiomFilter<OWLSubClassOfAxiom> implements
    AxiomRequirementClassFilter {

  private static final Logger logger = LoggerFactory
      .getLogger(AxiomRequirementSubClassOfFilter.class);

  public AxiomRequirementSubClassOfFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLSubClassOfAxiom buildAxiom(List<OWLClassExpression> classes) {
    return new OWLSubClassOfAxiomImpl(classes.get(0), classes.get(1), Collections.emptySet());
  }

  @Override
  protected void domain(List<OWLClassExpression> classes, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLClassExpression> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) == 0) {
      // Pivot is SubClass

      // Get the superclass
      Optional<OWLClassExpression> optSuperClass = match.getClassExpression(arguments.get(1));
      if (requirement.isAsserted().isPresent()) {
        if (requirement.isAsserted().get()) {
          if (optSuperClass.isPresent() && optSuperClass.get().isOWLClass()) {
            domain.retainAll(
                EntitySearcher.getSubClasses(optSuperClass.get().asOWLClass(), originalOntology)
                    .collect(Collectors.toSet()));
          } else {
            domain.retainAll(
                originalOntology.axioms(AxiomType.SUBCLASS_OF).map(OWLSubClassOfAxiom::getSubClass)
                    .collect(Collectors.toSet()));
          }
        } else if (optSuperClass.isPresent() && optSuperClass.get().isOWLClass()) {
          domain.removeAll(
              EntitySearcher.getSubClasses(optSuperClass.get().asOWLClass(), originalOntology)
                  .collect(Collectors.toSet()));
        }
      }

      if (requirement.isEntailed().isPresent() && optSuperClass.isPresent()) {
        Set<OWLClassExpression> superClasses = new HashSet<>();
        extendedReasoner.subClasses(optSuperClass.get()).map(this::revert)
            .forEach(superClasses::add);
        extendedReasoner.equivalentClasses(optSuperClass.get()).map(this::revert)
            .forEach(superClasses::add);
        if (requirement.isEntailed().get()) {
          domain.retainAll(superClasses);
        } else {
          domain.removeAll(superClasses);
        }
      }
    } else if (arguments.indexOf(pivot.getName()) == 1) {
      // Pivot is SuperClass

      // Get the SubClass
      Optional<OWLClassExpression> optSubClass = match.getClassExpression(arguments.get(0));

      if (requirement.isAsserted().isPresent()) {
        if (requirement.isAsserted().get()) {
          if (optSubClass.isPresent() && optSubClass.get().isOWLClass()) {
            domain.retainAll(
                EntitySearcher.getSuperClasses(optSubClass.get().asOWLClass(), originalOntology)
                    .collect(Collectors.toSet()));
          } else {
            domain.retainAll(originalOntology.axioms(AxiomType.SUBCLASS_OF)
                .map(OWLSubClassOfAxiom::getSuperClass).collect(Collectors.toSet()));
          }
        } else if (optSubClass.isPresent() && optSubClass.get().isOWLClass()) {
          domain.removeAll(
              EntitySearcher.getSubClasses(optSubClass.get().asOWLClass(), originalOntology)
                  .collect(Collectors.toSet()));
        }
      }

      if (requirement.isEntailed().isPresent() && optSubClass.isPresent()) {
        Set<OWLClassExpression> superClasses = new HashSet<>();
        extendedReasoner.superClasses(optSubClass.get()).map(this::revert)
            .forEach(superClasses::add);
        extendedReasoner.equivalentClasses(optSubClass.get()).map(this::revert)
            .forEach(superClasses::add);
        if (requirement.isEntailed().get()) {
          domain.retainAll(superClasses);
        } else {
          domain.removeAll(superClasses);
        }
      }
    }
  }
}
