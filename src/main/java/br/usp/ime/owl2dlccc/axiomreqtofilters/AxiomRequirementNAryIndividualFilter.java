/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public abstract class AxiomRequirementNAryIndividualFilter<T extends OWLIndividualAxiom> extends
    AxiomRequirementFilter implements AxiomRequirementIndividualFilter {

  public AxiomRequirementNAryIndividualFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void individualFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLIndividual> domain) {
    List<String> arguments = requirement.getAxiomSpecification().getArguments();
    List<OWLIndividual> individuals = arguments.stream()
        .map(match::getIndividual)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());
    domain(individuals, pivot, requirement, match, domain);
  }

  protected abstract T buildAxiom(List<OWLIndividual> individuals);

  protected abstract void domain(List<OWLIndividual> individuals, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match,
      Set<OWLIndividual> domain);
}
