/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AxiomRequirementClassAssertionFilter extends
    AxiomRequirementFilter implements AxiomRequirementIndividualFilter,
    AxiomRequirementClassFilter {

  private static final Logger logger = LoggerFactory
      .getLogger(AxiomRequirementClassAssertionFilter.class);

  public AxiomRequirementClassAssertionFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void classFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) != 0) {
      logger.debug("Pivot is not a class expression. Skipping...");
      return;
    }
    // The pivot is the class expression, so we get the individual
    Optional<OWLIndividual> optIndividual = match.getIndividual(arguments.get(1));

    Optional<OWLNamedIndividual> optNamedIndividual = optIndividual.filter(OWLIndividual::isNamed)
        .map(individual -> (OWLNamedIndividual) individual);

    if (requirement.isAsserted().isPresent()) {

      Set<OWLClassExpression> types = optNamedIndividual.map(owlNamedIndividual -> EntitySearcher
          .getTypes(owlNamedIndividual, originalOntology).collect(
              Collectors.toSet()))
          .orElseGet(() -> originalOntology.axioms(AxiomType.CLASS_ASSERTION).map(
              OWLClassAssertionAxiom::getClassExpression).collect(Collectors.toSet()));

      if (requirement.isAsserted().get()) {
        domain.retainAll(types);
      } else {
        domain.removeAll(types);
      }
    }

    if (requirement.isEntailed().isPresent() && optNamedIndividual.isPresent()) {
      Set<OWLClassExpression> types = Stream.of(optNamedIndividual.get())
          .map(extendedReasoner::getTypes).flatMap(NodeSet::entities).map(this::revert)
          .collect(Collectors.toSet());
      if (!types.isEmpty()) {
        if (requirement.isEntailed().get()) {
          domain.retainAll(types);
        } else {
          domain.removeAll(types);
        }
      }
    }
  }

  @Override
  public void individualFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLIndividual> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) != 1) {
      logger.debug("Pivot is not individual. Skipping...");
      return;
    }

    // The pivot is the individual, so we get the class expression
    Optional<OWLClassExpression> optClassExpression = match.getClassExpression(arguments.get(0));

    if (requirement.isAsserted().isPresent()) {
      Set<OWLIndividual> individuals;
      if (optClassExpression.isPresent() && optClassExpression.get().isOWLClass()) {
        individuals = EntitySearcher
            .getIndividuals(optClassExpression.get().asOWLClass(), originalOntology).collect(
                Collectors.toSet());
        if (requirement.isAsserted().get()) {
          domain.retainAll(individuals);
        } else {
          domain.removeAll(individuals);
        }
      } else if (requirement.isAsserted().get()) {
        domain.retainAll(originalOntology.axioms(AxiomType.CLASS_ASSERTION).map(
            OWLClassAssertionAxiom::getIndividual).collect(Collectors.toSet()));
      }

    }

    if (requirement.isEntailed().isPresent()) {
      if (optClassExpression.isPresent()) {
        Set<OWLNamedIndividual> individuals = extendedReasoner.instances(optClassExpression.get())
            .collect(Collectors.toSet());
        if (requirement.isEntailed().get()) {
          domain.retainAll(individuals);
        } else {
          domain.removeAll(individuals);
        }
      }
    }

  }
}
