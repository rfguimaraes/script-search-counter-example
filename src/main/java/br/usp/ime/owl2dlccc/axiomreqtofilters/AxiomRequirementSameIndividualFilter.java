/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLNaryIndividualAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLSameIndividualAxiomImpl;

public class AxiomRequirementSameIndividualFilter extends
    AxiomRequirementNAryIndividualFilter<OWLSameIndividualAxiom> {

  public AxiomRequirementSameIndividualFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLSameIndividualAxiom buildAxiom(List<OWLIndividual> individuals) {
    return new OWLSameIndividualAxiomImpl(individuals, Collections.emptySet());
  }

  @Override
  protected void domain(List<OWLIndividual> individuals, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLIndividual> domain) {

    if (requirement.isAsserted().orElse(false)) {
      if (individuals.isEmpty()) {
        domain.retainAll(
            originalOntology.axioms(AxiomType.SAME_INDIVIDUAL)
                .flatMap(OWLNaryIndividualAxiom::individuals)
                .collect(Collectors.toSet()));
      } else {
        domain.retainAll(EntitySearcher.getSameIndividuals(individuals.get(0), originalOntology)
            .collect(Collectors.toSet()));
      }
    } else if (!individuals.isEmpty()) {
      domain.removeAll(EntitySearcher.getSameIndividuals(individuals.get(0), originalOntology)
          .collect(Collectors.toSet()));
    }

    List<OWLNamedIndividual> named = individuals.stream().filter(OWLIndividual::isNamed)
        .map(ind -> (OWLNamedIndividual) ind).collect(Collectors.toList());

    if (requirement.isEntailed().isPresent() && !named.isEmpty()) {
      Set<OWLNamedIndividual> sameIndividuals = extendedReasoner.sameIndividuals(named.get(0))
          .collect(Collectors.toSet());
      if (requirement.isEntailed().get()) {
        domain.retainAll(sameIndividuals);
      } else {
        domain.removeAll(sameIndividuals);
      }
    }
  }
}
