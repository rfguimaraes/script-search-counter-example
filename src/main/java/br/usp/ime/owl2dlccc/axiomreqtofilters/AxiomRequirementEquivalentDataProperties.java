/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.AsOWLDataProperty;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLNaryPropertyAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLEquivalentDataPropertiesAxiomImpl;

public class AxiomRequirementEquivalentDataProperties extends
    AxiomRequirementNAryDataPropertyFilter<OWLEquivalentDataPropertiesAxiom> {

  public AxiomRequirementEquivalentDataProperties(OWLOntology ontology, OWLReasoner reasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLEquivalentDataPropertiesAxiom buildAxiom(
      List<OWLDataPropertyExpression> properties) {
    return new OWLEquivalentDataPropertiesAxiomImpl(properties, Collections.emptySet());
  }

  @Override
  public void dataPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLDataPropertyExpression> domain) {

    List<OWLDataProperty> properties = requirement.getAxiomSpecification().getArguments()
        .stream().map(match::getDataPropertyExpression)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .filter(AsOWLDataProperty::isOWLDataProperty) //Currently, it should always be true
        .map(AsOWLDataProperty::asOWLDataProperty)
        .collect(Collectors.toList());

    if (requirement.isAsserted().isPresent()) {
      if (requirement.isAsserted().get()) {
        // All undefined, but we can still look at those referenced by some axiom of that type
        if (properties.isEmpty()) {
          domain.retainAll(originalOntology.axioms(AxiomType.EQUIVALENT_DATA_PROPERTIES).flatMap(
              OWLNaryPropertyAxiom::properties).collect(Collectors.toSet()));
        } else {
          domain.retainAll(
              EntitySearcher.getEquivalentProperties(properties.get(0), originalOntology)
                  .collect(Collectors.toSet()));
        }
      } else if (!properties.isEmpty()) {
        // It can't be any other property that appears in a equivalence with one of the others
        domain.removeAll(originalOntology.axioms(AxiomType.EQUIVALENT_DATA_PROPERTIES).filter(
            ax -> properties.stream().anyMatch(prop -> ax.properties().anyMatch(prop::equals)))
            .flatMap(OWLNaryPropertyAxiom::properties).collect(Collectors.toSet()));
      }
    }

    // Since we are looking at equivalences, as long as we have one defined we can filter
    // both positive and negative entailment requests.
    if (requirement.isEntailed().isPresent() && !properties.isEmpty()) {
      Set<OWLDataProperty> equivalents = extendedReasoner
          .equivalentDataProperties(properties.get(0)).collect(Collectors.toSet());
      if (requirement.isEntailed().get()) {
        domain.retainAll(equivalents);
      } else {
        domain.removeAll(equivalents);
      }
    }
  }
}
