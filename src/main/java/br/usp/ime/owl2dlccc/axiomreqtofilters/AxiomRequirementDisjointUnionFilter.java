/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import uk.ac.manchester.cs.owl.owlapi.OWLDisjointUnionAxiomImpl;

public class AxiomRequirementDisjointUnionFilter extends
    AxiomRequirementClassAxiomFilter<OWLDisjointUnionAxiom> implements AxiomRequirementClassFilter {

  public AxiomRequirementDisjointUnionFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  /*public void classFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> include, Set<OWLClassExpression> exclude) {
    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    int pivotIndex = primitiveTerms.indexOf(pivot);

    Set<OWLClassExpression> defined = requirement.getAxiomSpecification()
        .getArguments().stream().map(termMap::get)
        .filter(term -> primitiveTerms.indexOf(term) < pivotIndex)
        .map(term -> match.getClassExpression(term.getName()))
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toSet());

    if (arguments.indexOf(pivot.getName()) == 0) {
      // Pivot is SuperClass
      // Get the defined partitions


      if (defined.isEmpty()) {
        // TODO: warning or exception
        return;
      }
      if (requirement.isAsserted().isPresent()) {
        if (requirement.isAsserted().get()) {
          // Add every candidate for the union of the partitions
          originalOntology.axioms(AxiomType.DISJOINT_UNION)
              .filter(axiom ->
                  axiom.classExpressions().collect(Collectors.toSet()).containsAll(defined))
              .map(OWLDisjointUnionAxiom::getOWLClass).forEach(include::add);
        } else if (defined.size() == ImmutableSet.of(arguments).size() - 1) {
          // It is not straightforward to select what to exclude in this case,
          // unless everything else is defined, so that we can look for the whole axiom
          originalOntology.axioms(AxiomType.DISJOINT_UNION)
              .filter(axiom ->
                  axiom.classExpressions().collect(Collectors.toSet()).equals(defined))
              .map(OWLDisjointUnionAxiom::getOWLClass).forEach(exclude::add);
        }
      }
      if (requirement.isEntailed().isPresent()) {
        Set<OWLClassExpression> destination = (requirement.isEntailed().get()) ? include : exclude;
        defined.stream().flatMap(extendedReasoner::disjointClasses).forEach(destination::add);
      }

    } else {
      // Pivot is one of the partitions
      if (requirement.isAsserted().isPresent()) {
        if (requirement.isAsserted().get()) {
          originalOntology.axioms(AxiomType.DISJOINT_UNION)
              .filter(axiom -> axiom.signature().collect(Collectors.toSet()).containsAll(defined))
              .flatMap(OWLDisjointUnionAxiom::classExpressions)
              .filter(ce -> !defined.contains(ce))
              .forEach(include::add);
        } else if (defined.size() == ImmutableSet.of(arguments).size() - 1) {
          // It is not straightforward to select what to exclude in this case,
          // unless everything else but the pivot is defined,
          // so that we can look for the whole axiom
          Optional<OWLClassExpression> optUnion = match.getClassExpression(arguments.get(0));
          if (!optUnion.isPresent()) {
            return;
          }
          originalOntology.disjointClassesAxioms(optUnion.get().asOWLClass())
              .filter(axiom -> axiom.signature().collect(Collectors.toSet()).containsAll(defined))
              .filter(axiom -> axiom.classExpressions().count() == ImmutableSet.of(arguments).size() - 2)
              .flatMap(OWLNaryClassAxiom::classExpressions)
              .filter(ce -> !defined.contains(ce))
              .forEach(exclude::add);
        }
      }
      if (requirement.isEntailed().isPresent()) {
        // TODO: fix the negative side as for Equiv. Classes
        Set<OWLClassExpression> destination = (requirement.isEntailed().get()) ? include : exclude;
        defined.stream().flatMap(extendedReasoner::disjointClasses).forEach(destination::add);
      }
    }
  }*/

  @Override
  protected OWLDisjointUnionAxiom buildAxiom(List<OWLClassExpression> classes) {
    return new OWLDisjointUnionAxiomImpl(classes.get(0).asOWLClass(), classes.stream().skip(1).collect(Collectors.toList()), Collections .emptySet());
  }

  @Override
  protected void domain(List<OWLClassExpression> classes, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLClassExpression> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) == 0) {
      // Pivot is the partitioned set (head)
      if (requirement.isAsserted().isPresent()) {
        Set<OWLClass> partitionHeads = originalOntology.axioms(AxiomType.DISJOINT_UNION)
            .filter(ax -> classes.stream()
                .allMatch(ce -> ce.signature()
                    .allMatch(ax::containsEntityInSignature)))
            .map(OWLDisjointUnionAxiom::getOWLClass)
            .collect(Collectors.toSet());
        if (requirement.isAsserted().get()) {
          domain.retainAll(partitionHeads);
        } else if (classes.size() == arguments.size() - 1) {
          // Only the head left to define, hence we can exclude safely
          domain.removeAll(partitionHeads);
        }
      }
      if (requirement.isEntailed().isPresent()) {
        if (requirement.isEntailed().get()) {

          Set<OWLClassExpression> intersection = classes.stream().skip(1).map(
              ce -> Sets.union(extendedReasoner.superClasses(ce)
                      .collect(Collectors.toSet()),
                  extendedReasoner.equivalentClasses(ce).collect(Collectors.toSet())))
              .collect(
                  () -> Sets.newHashSet(Sets.union(extendedReasoner.superClasses(classes.get(0))
                          .collect(Collectors.toSet()),
                      extendedReasoner.equivalentClasses(classes.get(0))
                          .collect(Collectors.toSet()))),
                  Set::retainAll, Set::retainAll);

          /* The non-entailment case is really non-trivial, as the candidates for the head
          still might not be entailed as a head for a disjoint union (they might be only
          superclasses for all classes given. */
          if (requirement.isEntailed().get()) {
            domain.retainAll(intersection.stream().map(this::revert).collect(Collectors.toSet()));
          }
        }
      }
    } else {
      // Pivot is a partition
      if (requirement.isAsserted().isPresent()) {
        if (requirement.isAsserted().get()) {
          if (classes.isEmpty()) {
            // All undefined, but we can still look at those referenced by some axiom of that type
            domain.retainAll(originalOntology.axioms(AxiomType.DISJOINT_UNION).flatMap(
                OWLDisjointUnionAxiom::classExpressions).collect(
                Collectors.toSet()));
          } else {
            // Check if the "head" is defined
            Optional<OWLClassExpression> optHead = match
                .getClassExpression(arguments.get(0));
            Set<OWLClassExpression> candidates = Sets.newHashSet();
            optHead.ifPresent(owlClassExpression -> candidates
                .addAll(originalOntology.axioms(AxiomType.DISJOINT_UNION)
                    .filter(ax -> ax.getOWLClass().equals(owlClassExpression)).flatMap(
                        OWLDisjointUnionAxiom::classExpressions).collect(
                        Collectors.toSet())));

            List<OWLClassExpression> otherPartitions = Lists.newArrayList(classes);
            optHead.ifPresent(head -> otherPartitions.removeIf(x -> x.equals(head)));

            candidates.addAll(originalOntology.axioms(AxiomType.DISJOINT_UNION).filter(
                ax -> otherPartitions.stream()
                    .allMatch(c -> ax.classExpressions().anyMatch(c::equals))).flatMap(
                OWLDisjointUnionAxiom::classExpressions).collect(
                Collectors.toSet()));

            domain.retainAll(candidates);
          }
        } else if (classes.size() == arguments.size() - 1) {
          // We can only exclude individuals that surely would make the axiom to be an asserted one,
          // hence from the n different individuals n - 1 must be defined already
          domain.removeAll(originalOntology.axioms(AxiomType.DISJOINT_UNION).filter(
              ax -> classes.stream().allMatch(c -> ax.classExpressions().anyMatch(c::equals)))
              .flatMap(
                  OWLDisjointUnionAxiom::classExpressions).collect(
                  Collectors.toSet()));
        }
      }

      // For entailment we need at least one class
      if (requirement.isEntailed().isPresent() && !classes.isEmpty()) {
        Set<OWLClassExpression> subOfHead = Sets.newHashSet();
        Optional<OWLClassExpression> optHead = match
            .getClassExpression(arguments.get(0));

        if (optHead.isPresent()) {
          subOfHead.addAll(extendedReasoner.subClasses(optHead.get()).map(this::revert)
              .collect(Collectors.toSet()));
          subOfHead.addAll(extendedReasoner.equivalentClasses(optHead.get()).map(this::revert)
              .collect(Collectors.toSet()));
        }

        List<OWLClassExpression> otherPartitions = Lists.newArrayList(classes);
        optHead.ifPresent(head -> otherPartitions.removeIf(x -> x.equals(head)));

        Set<OWLClassExpression> intersection = otherPartitions.stream().skip(1).map(
            ce -> extendedReasoner.disjointClasses(ce)
                .collect(Collectors.toSet()))
            .collect(() -> Sets.newHashSet(
                extendedReasoner.disjointClasses(classes.get(0)).collect(Collectors.toSet())),
                Set::retainAll, Set::retainAll);

        intersection = intersection.stream().map(this::revert).collect(Collectors.toSet());

        if (requirement.isEntailed().get()) {
          domain.retainAll(Sets.union(subOfHead, intersection));
        } else if (classes.size() == arguments.size() - 1) {
          /* We can only really exclude if all classes but one partition (the pivot) are defined
          In this case, we cannot take a class that is at the same time a subclass of the head and
          disjoint with every other partition. */
          domain.removeAll(Sets.intersection(subOfHead, intersection));
        }

      }
    }
  }
}
