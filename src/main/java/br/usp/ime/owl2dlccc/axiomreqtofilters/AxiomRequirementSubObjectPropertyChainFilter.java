/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import uk.ac.manchester.cs.owl.owlapi.OWLSubPropertyChainAxiomImpl;

public class AxiomRequirementSubObjectPropertyChainFilter extends
    AxiomRequirementNAryObjectPropertyFilter<OWLSubPropertyChainOfAxiom> {

  public AxiomRequirementSubObjectPropertyChainFilter(OWLOntology ontology, OWLReasoner reasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected void domain(List<OWLObjectPropertyExpression> properties, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLObjectPropertyExpression> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (requirement.isAsserted().isPresent()) {
      if (requirement.isAsserted().get()) {

        if (arguments.indexOf(pivot.getName()) == arguments.size() - 1) {
          // Pivot is the superproperty
          domain.retainAll(originalOntology.axioms(AxiomType.SUB_PROPERTY_CHAIN_OF)
              .map(OWLSubPropertyChainOfAxiom::getSuperProperty).collect(
                  Collectors.toSet()));
        } else {
          domain.retainAll(originalOntology.axioms(AxiomType.SUB_PROPERTY_CHAIN_OF)
              .map(OWLSubPropertyChainOfAxiom::getPropertyChain).flatMap(
                  Collection::stream).collect(Collectors.toSet()));
        }
      }
    }
  }

  @Override
  protected OWLSubPropertyChainOfAxiom buildAxiom(List<OWLObjectPropertyExpression> properties) {
    return new OWLSubPropertyChainAxiomImpl(properties.subList(0, properties.size() - 1),
        properties.get(properties.size() - 1), Collections.emptySet());
  }
}
