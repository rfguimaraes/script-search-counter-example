/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.Set;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class EntityDomainFilter {

  private final Map<AxiomType, ? extends AxiomRequirementFilter> filters;

  public EntityDomainFilter(OWLOntology originalOntology, OWLReasoner extendedReasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    filters = ImmutableMap.<AxiomType, AxiomRequirementFilter>builder()
        .put(AxiomType.CLASS_ASSERTION,
            new AxiomRequirementClassAssertionFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.SAME_INDIVIDUAL,
            new AxiomRequirementSameIndividualFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DIFFERENT_INDIVIDUALS,
            new AxiomRequirementDifferentIndividualsFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.HAS_KEY,
            new AxiomRequirementHasKeyFilter(originalOntology, extendedReasoner, extensionBiMap))
        .put(AxiomType.SUBCLASS_OF,
            new AxiomRequirementSubClassOfFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.EQUIVALENT_CLASSES,
            new AxiomRequirementEquivalentClassesFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DISJOINT_CLASSES,
            new AxiomRequirementDisjointClassesFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DISJOINT_UNION,
            new AxiomRequirementDisjointUnionFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.OBJECT_PROPERTY_DOMAIN,
            new AxiomRequirementObjectPropertyDomainFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DATA_PROPERTY_DOMAIN,
            new AxiomRequirementObjectPropertyDomainFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.OBJECT_PROPERTY_RANGE,
            new AxiomRequirementObjectPropertyRangeFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.FUNCTIONAL_OBJECT_PROPERTY,
            new AxiomRequirementFunctionalObjectPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.FUNCTIONAL_DATA_PROPERTY,
            new AxiomRequirementFunctionalDataPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY,
            new AxiomRequirementInverseFunctionalObjectPropertyFilter(originalOntology,
                extendedReasoner, extensionBiMap))
        .put(AxiomType.REFLEXIVE_OBJECT_PROPERTY,
            new AxiomRequirementReflexiveObjectPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.IRREFLEXIVE_OBJECT_PROPERTY,
            new AxiomRequirementIrreflexiveObjectPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.TRANSITIVE_OBJECT_PROPERTY,
            new AxiomRequirementTransitiveObjectProperty(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.SYMMETRIC_OBJECT_PROPERTY,
            new AxiomRequirementSymmetricObjectPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.ASYMMETRIC_OBJECT_PROPERTY,
            new AxiomRequirementAsymmetricObjectPropertyFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.SUB_OBJECT_PROPERTY,
            new AxiomRequirementSubObjectPropertyOfFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.SUB_DATA_PROPERTY,
            new AxiomRequirementSubDataPropertyOfFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
            new AxiomRequirementEquivalentObjectProperties(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.EQUIVALENT_DATA_PROPERTIES,
            new AxiomRequirementEquivalentDataProperties(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.SUB_PROPERTY_CHAIN_OF,
            new AxiomRequirementSubObjectPropertyChainFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.INVERSE_OBJECT_PROPERTIES,
            new AxiomRequirementInverseObjectPropertiesFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DISJOINT_OBJECT_PROPERTIES,
            new AxiomRequirementDisjointObjectPropertiesFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .put(AxiomType.DISJOINT_DATA_PROPERTIES,
            new AxiomRequirementDisjointDataPropertiesFilter(originalOntology, extendedReasoner,
                extensionBiMap))
        .build();
  }

  public void cutClassDomain(Set<OWLClassExpression> domain, AxiomRequirement requirement,
      PrimitiveTerm pivot, Match match) {
    AxiomType axiomType = requirement.getAxiomSpecification().getAxiomType();
    AxiomRequirementFilter requirementFilter = filters.get(axiomType);

    if (requirementFilter instanceof AxiomRequirementClassFilter) {
      ((AxiomRequirementClassFilter) requirementFilter)
          .classFilter(pivot, requirement, match, domain);
    }
  }

  public void cutObjectPropertyDomain(Set<OWLObjectPropertyExpression> domain,
      AxiomRequirement requirement, PrimitiveTerm pivot, Match match) {
    AxiomType axiomType = requirement.getAxiomSpecification().getAxiomType();
    AxiomRequirementFilter requirementFilter = filters.get(axiomType);

    if (requirementFilter instanceof AxiomRequirementObjectPropertyFilter) {
      ((AxiomRequirementObjectPropertyFilter) requirementFilter)
          .objectPropertyFilter(pivot, requirement, match, domain);
    }
  }

  public void cutDataPropertyDomain(Set<OWLDataPropertyExpression> domain,
      AxiomRequirement requirement, PrimitiveTerm pivot, Match match) {
    AxiomType axiomType = requirement.getAxiomSpecification().getAxiomType();
    AxiomRequirementFilter requirementFilter = filters.get(axiomType);

    if (requirementFilter instanceof AxiomRequirementDataPropertyFilter) {
      ((AxiomRequirementDataPropertyFilter) requirementFilter)
          .dataPropertyFilter(pivot, requirement, match, domain);
    }
  }

  public void cutIndividualDomain(Set<OWLIndividual> domain, AxiomRequirement requirement,
      PrimitiveTerm pivot, Match match) {
    AxiomType axiomType = requirement.getAxiomSpecification().getAxiomType();
    AxiomRequirementFilter requirementFilter = filters.get(axiomType);

    if (requirementFilter instanceof AxiomRequirementIndividualFilter) {
      ((AxiomRequirementIndividualFilter) requirementFilter)
          .individualFilter(pivot, requirement, match, domain);
    }
  }
}
