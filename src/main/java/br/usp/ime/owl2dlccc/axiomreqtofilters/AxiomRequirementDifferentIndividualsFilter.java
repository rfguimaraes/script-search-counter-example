/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AsOWLNamedIndividual;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLNaryIndividualAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLDifferentIndividualsAxiomImpl;

public class AxiomRequirementDifferentIndividualsFilter extends
    AxiomRequirementNAryIndividualFilter<OWLDifferentIndividualsAxiom> {

  public AxiomRequirementDifferentIndividualsFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLDifferentIndividualsAxiom buildAxiom(List<OWLIndividual> individuals) {
    return new OWLDifferentIndividualsAxiomImpl(individuals, Collections.emptySet());
  }

  @Override
  protected void domain(List<OWLIndividual> individuals, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLIndividual> domain) {

    int totalIndividuals = requirement.getAxiomSpecification().getArguments().size();

    if (requirement.isAsserted().isPresent()) {
      if (individuals.isEmpty()) {
        // All undefined, but we can still look at those referenced by some axiom of that type
        if (requirement.isAsserted().get()) {
          domain.retainAll(originalOntology.axioms(AxiomType.DIFFERENT_INDIVIDUALS).flatMap(
              OWLNaryIndividualAxiom::individuals).collect(Collectors.toSet()));
        }
      } else {
        Set<OWLIndividual> intersection = individuals.stream().skip(1).map(
            ind -> EntitySearcher.getDifferentIndividuals(ind, originalOntology)
                .collect(Collectors.toSet()))
            .collect(() -> Sets.newHashSet(
                EntitySearcher.getDifferentIndividuals(individuals.get(0), originalOntology)
                    .collect(
                        Collectors.toSet())), Set::retainAll, Set::retainAll);
        if (requirement.isAsserted().get()) {
          // At least one individual is defined and we want the axiom to be asserted
          domain.retainAll(intersection);
        } else if (individuals.size() == totalIndividuals - 1) {
          // We can only exclude individuals that surely would make the axiom to be an asserted one,
          // hence from the n different individuals n - 1 must be defined already
          domain.removeAll(intersection);
        }
      }
    }

    // We need at least one individual to be able to check via entailments
    if (requirement.isEntailed().isPresent() && !individuals.isEmpty()) {
      List<OWLNamedIndividual> named = individuals.stream()
          .filter(AsOWLNamedIndividual::isOWLNamedIndividual)
          .map(AsOWLNamedIndividual::asOWLNamedIndividual)
          .collect(Collectors.toList());
      Set<OWLIndividual> intersection = named.stream().skip(1).map(
          ind -> extendedReasoner.differentIndividuals(ind)
              .collect(Collectors.toSet()))
          .collect(() -> Sets.newHashSet(
              extendedReasoner.differentIndividuals(named.get(0)).collect(
                  Collectors.toSet())), Set::retainAll, Set::retainAll);
      if (requirement.isEntailed().get()) {
        domain.retainAll(intersection);
      } else if (individuals.size() == totalIndividuals - 1) {
        // We can only really exclude if all but one individual (the pivot) are defined
        domain.removeAll(intersection);
      }
    }
  }
}
