/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import com.google.common.collect.BiMap;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public abstract class AxiomRequirementFilter {

  protected final OWLOntology originalOntology;
  protected final OWLReasoner extendedReasoner;
  private final BiMap<OWLClass, OWLClassExpression> extensionBiMap;

  public AxiomRequirementFilter(OWLOntology originalOntology, OWLReasoner extendedReasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    this.originalOntology = originalOntology;
    this.extendedReasoner = extendedReasoner;
    this.extensionBiMap = extensionBiMap;
  }

  protected OWLClassExpression revert(OWLClassExpression classExpression) {
    if (classExpression.isOWLClass()) {
      OWLClassExpression toReturn = extensionBiMap.get(classExpression.asOWLClass());
      if (toReturn != null) {
        return toReturn;
      }
    }
    return classExpression;
  }

  protected OWLClassExpression revert(OWLClass classExpression) {
    OWLClassExpression toReturn = extensionBiMap.get(classExpression);
    if (toReturn != null) {
      return toReturn;
    } else {
      return classExpression;
    }
  }
}
