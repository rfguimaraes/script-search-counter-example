/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.HasProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyCharacteristicAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public abstract class AxiomRequirementObjectPropertyCharacteristicFilter<T extends OWLObjectPropertyCharacteristicAxiom> extends
    AxiomRequirementFilter implements AxiomRequirementObjectPropertyFilter {

  public AxiomRequirementObjectPropertyCharacteristicFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void objectPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLObjectPropertyExpression> domain) {
    if (requirement.isAsserted().isPresent()) {
      Set<OWLObjectPropertyExpression> properties = originalOntology
          .axioms(getType()).map(HasProperty::getProperty)
          .collect(Collectors.toSet());
      if (requirement.isAsserted().get()) {
        domain.retainAll(properties);
      } else {
        domain.removeAll(properties);
      }
    }
  }

  protected abstract AxiomType<T> getType();
}
