/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.semanticweb.owlapi.model.AsOWLClass;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLNaryClassAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLDisjointClassesAxiomImpl;

public class AxiomRequirementDisjointClassesFilter extends
    AxiomRequirementClassAxiomFilter<OWLDisjointClassesAxiom> implements
    AxiomRequirementClassFilter {

  public AxiomRequirementDisjointClassesFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLDisjointClassesAxiom buildAxiom(List<OWLClassExpression> classes) {
    return new OWLDisjointClassesAxiomImpl(classes, Collections.emptySet());
  }

  private Set<OWLClassExpression> getDisjoints(OWLClass owlClass, OWLOntology ontology) {
    Set<OWLClassExpression> disjoints = EntitySearcher.getDisjointClasses(owlClass, originalOntology).collect(Collectors.toSet());
    disjoints.remove(owlClass);
    return disjoints;
  }

  @Override
  protected void domain(List<OWLClassExpression> classes, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLClassExpression> domain) {

    int totalClasses = requirement.getAxiomSpecification().getArguments().size();

    // The negative cases are not straightforward, we would have to check whether
    // there is only one concept left to be defined and all others are disjoint.
    if (requirement.isAsserted().isPresent()) {
      if (requirement.isAsserted().get()) {
        if (classes.isEmpty()) {
          // All undefined, but we can still look at those referenced by some axiom of that type
          domain.retainAll(originalOntology.axioms(AxiomType.DISJOINT_CLASSES)
              .flatMap(OWLNaryClassAxiom::classExpressions).collect(Collectors.toSet()));
        } else {
          List<OWLClass> owlClasses = classes.stream().filter(AsOWLClass::isOWLClass)
              .map(AsOWLClass::asOWLClass).collect(
                  Collectors.toList());
          if (!owlClasses.isEmpty()) {

            Set<OWLClassExpression> disjoints = getDisjoints(owlClasses.get(0).asOWLClass(), originalOntology);

            Iterator<OWLClass> iterator = owlClasses.iterator();
            iterator.next();

            while (iterator.hasNext()) {
              disjoints.retainAll(getDisjoints(iterator.next().asOWLClass(), originalOntology));
            }

            if (requirement.isAsserted().get()) {
              // May only choose among the classes that intersect all of the already defined terms
              domain.retainAll(disjoints);
            } else if (classes.size() == totalClasses - 1) {
              // All other terms are defined and we cannot be disjoint with all of them
              domain.removeAll(disjoints);
            }
          }
        }
      }
    }

    // For entailment we need at least one class
    if (requirement.isEntailed().isPresent() && !classes.isEmpty()) {
      Set<OWLClassExpression> intersection = classes.stream().skip(1).map(
          ce -> extendedReasoner.disjointClasses(ce).map(this::revert).collect(Collectors.toSet()))
          .collect(() -> Sets.newHashSet(
              extendedReasoner.disjointClasses(classes.get(0)).map(this::revert)
                  .collect(Collectors.toSet())), Set::retainAll, Set::retainAll);
      if (requirement.isEntailed().get()) {
        domain.retainAll(intersection);
      } else if (classes.size() == totalClasses - 1) {
        // We can only really exclude if all but one individual (the pivot) are defined
        domain.removeAll(intersection);
      }
    }
  }
}
