/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.ConditionChecker.CheckPhase;
import br.usp.ime.owl2dlccc.ConditionChecker.ConditionCheckResult;
import br.usp.ime.owl2dlccc.axiomspectoaxiom.AxiomSpecificationToOWLAxiomTranslator;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.CatalystBlock;
import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.IntegrityConstraints;
import br.usp.ime.owl2dlccc.model.OWL2DLFragment;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.Sets;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.io.OWLOntologyDocumentTarget;
import org.semanticweb.owlapi.io.WriterDocumentTarget;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The main class that starts the procedure to generate cases.
 *
 * @author Ricardo F. Guimarães
 */
public class ChangeCaseCreator {

  public static class TimingThread extends Thread {

    private final long timeoutMilliseconds;

    public TimingThread(long timeoutMilliseconds) {
      super("Timing Thread " + timeoutMilliseconds + "ms");
      this.timeoutMilliseconds = timeoutMilliseconds;
    }

    @Override
    public void run() {
      try {
        Thread.sleep(timeoutMilliseconds);
        throw new RuntimeException("Timeout!");
      } catch (InterruptedException e) {
        logger.info("Woken up, true task already finished.");
      }
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(ChangeCaseCreator.class);

  public static boolean apply(OWLOntology ontology, String sourcePath,
      ChangeSpecification changeSpecification,
      OWLReasonerFactory reasonerFactory, Path outputDir, String runName,
      DistinctnessPolicy distinctnessPolicy, Integer iterations, long seed, @Nullable Long
      timeoutMilliseconds)
      throws IOException, OWLOntologyStorageException, OWLOntologyCreationException {

    logger.debug("Start processing");
    OWLReasonerConfiguration reasonerConfiguration =
        (timeoutMilliseconds == null) ? new SimpleConfiguration()
            : new SimpleConfiguration(timeoutMilliseconds);
    OWLReasoner preReasoner = reasonerFactory.createReasoner(ontology, reasonerConfiguration);
    logger.debug("Preconditions reasoner created");
    int currentIteration = 1;

    // Check input fragment

    OWL2DLFragment fragment = changeSpecification.getHeaderBlock().getInputFragment()
        .orElse(OWL2DLFragment.DL);

    if (!fragment.check(ontology).isInProfile()) {
      logger.error("Input ontology is not in the required fragment {}.", fragment.toString());
      return false;
    }

    // Prepare match generation
    logger.debug("Prepare match generation");

    List<AxiomRequirement> axiomRequirements = changeSpecification.getPreconditions()
        .map(ic -> ic.getAxiomRequirements().orElse(Collections.emptyList()))
        .orElse(Collections.emptyList());

    List<PrimitiveTerm> primitiveTerms = changeSpecification.getTermsBlock()
        .map(tb -> tb.getPrimitives().orElse(Collections.emptyList()))
        .orElse(Collections.emptyList());

    List<CompositeTerm> compositeTerms = changeSpecification.getTermsBlock()
        .map(tb -> tb.getComposites().orElse(Collections.emptyList()))
        .orElse(Collections.emptyList());

    DomainComputer domainComputer = new DomainComputer(ontology, reasonerFactory,
        new AxiomRequirementNetwork(axiomRequirements, primitiveTerms));

    FullMatchFactory matchIterator = new FullMatchFactory(primitiveTerms, compositeTerms,
        distinctnessPolicy, domainComputer, seed);

    // Iterate over matches and evaluate them

    while (matchIterator.hasNext() && currentIteration <= iterations) {

      MatchSearchResult result;
      ExecutorService executorService = Executors.newSingleThreadExecutor();
      Future<MatchSearchResult> runResult = executorService.submit(
          () ->
              findMatch(matchIterator, changeSpecification, preReasoner, ontology, reasonerFactory,
                  timeoutMilliseconds));
      try {
        if (timeoutMilliseconds != null) {
          Thread thread = new TimingThread((long) (timeoutMilliseconds * 1.2));
          thread.start();
          result = runResult.get(timeoutMilliseconds, TimeUnit.MILLISECONDS);
          thread.interrupt();
        } else {
          result = runResult.get();
        }
        if (result.getMatch().isPresent() && result.getNewOntology().isPresent()) {
          // Everything is OK, ready to save!

          if (generateOutputFiles(sourcePath, changeSpecification, outputDir, runName,
              distinctnessPolicy, seed, currentIteration, result.getMatch().get(),
              result.getNewOntology().get())) {
            logger.info("Successfully written case {}/{}", currentIteration++, iterations);
          } else {
            logger.error("Failed to generate output files. Aborting.");
          }
        }
      } catch (InterruptedException | ExecutionException | TimeoutException e) {
        logger.error("Error during match search: {}", e.getMessage());
      } finally {
        executorService.shutdownNow();
      }
    }
    preReasoner.dispose();
    return currentIteration > iterations;
  }

  static MatchSearchResult findMatch(FullMatchFactory matchFactory,
      ChangeSpecification changeSpecification, OWLReasoner preReasoner, OWLOntology ontology,
      OWLReasonerFactory reasonerFactory, Long timeout) {

    Match match = matchFactory.next();

    logger.debug("Trying match: {}", match);

    // Check preconditions

    if (changeSpecification.getPreconditions().isPresent()) {

      ConditionCheckResult preconditionResult = verifyPreconditions(ontology,
          changeSpecification.getPreconditions().get(), preReasoner, match);

      if (!preconditionResult.passed()) {
        logger.debug("Precondition failed at {} phase", preconditionResult.getCheckPhase());
        if (!Sets.newHashSet(CheckPhase.COHERENCE, CheckPhase.CONSISTENCY)
            .contains(preconditionResult.getCheckPhase())) {
          matchFactory.update(preconditionResult.getFailedRequirement());
        }
        return new MatchSearchResult(null, null);
      }
    }

    // Apply actions

    OWLOntology newOntology = null;
    if (changeSpecification.getActionBlock().isPresent()) {
      ActionManager actionManager = new ActionManager(
          changeSpecification.getActionBlock().get(), ontology, match);
      try {
        newOntology = actionManager.applyChanges();
      } catch (OWLOntologyCreationException | SpecificationValidationException e) {
        logger.error("Failed to create new ontology: {}", e.getMessage());
        return new MatchSearchResult(null, null);
      }
    } else {
      newOntology = ontology;
    }

    // Check output fragment

    OWL2DLFragment fragment = changeSpecification.getHeaderBlock().getOutputFragment()
        .orElse(OWL2DLFragment.DL);

    if (!fragment.check(newOntology).isInProfile()) {
      logger.debug("Modified ontology is not in the required fragment {}.", fragment.toString());
      return new MatchSearchResult(null, null);
    }

    // Verify post-conditions

    if (changeSpecification.getPostconditions().isPresent()) {
      ConditionCheckResult postconditionResult = verifyPostconditions(newOntology,
          changeSpecification.getPostconditions().get(), reasonerFactory, match, timeout);

      if (!postconditionResult.passed()) {
        logger.debug("Postcondition failed at {} phase", postconditionResult.getCheckPhase());
        if (!Sets.newHashSet(CheckPhase.COHERENCE, CheckPhase.CONSISTENCY)
            .contains(postconditionResult.getCheckPhase())) {
          matchFactory.update(postconditionResult.getFailedRequirement());
        }
        return new MatchSearchResult(null, null);
      }
    }

    return new MatchSearchResult(match, newOntology);
  }

  private static ConditionCheckResult verifyPreconditions(OWLOntology ontology,
      IntegrityConstraints preconditions, OWLReasoner reasoner, Match match) {
    ConditionChecker conditionChecker = new ConditionChecker(ontology, reasoner, match);
    return conditionChecker.check(preconditions);
  }

  private static ConditionCheckResult verifyPostconditions(OWLOntology ontology,
      IntegrityConstraints postconditions, OWLReasonerFactory reasonerFactory, Match match,
      Long timeoutMilliseconds) {

    OWLReasonerConfiguration reasonerConfiguration =
        (timeoutMilliseconds == null) ? new SimpleConfiguration()
            : new SimpleConfiguration(timeoutMilliseconds);
    OWLReasoner posReasoner = reasonerFactory.createReasoner(ontology, reasonerConfiguration);
    ConditionChecker conditionChecker = new ConditionChecker(ontology, posReasoner, match);
    ConditionCheckResult postconditionResult = conditionChecker.check(postconditions);
    posReasoner.dispose();
    return postconditionResult;

  }

  private static boolean generateOutputFiles(String sourcePath,
      ChangeSpecification changeSpecification, Path outputDir, String runName,
      DistinctnessPolicy distinctnessPolicy, long seed, int currentIteration,
      Match match, OWLOntology newOntology)
      throws IOException, OWLOntologyStorageException {
    saveModifiedOntology(newOntology, runName, outputDir, currentIteration);

    if (changeSpecification.getCatalysts().isEmpty()) {
      CaseInstance caseInstance = new CaseInstance(
              runName,
              null,
              null,
              null,
              null,
              null,
              newOntology,
              Paths.get(sourcePath),
              changeSpecification.getHeaderBlock().getName(),
              outputDir,
              distinctnessPolicy.getMultiplicity(),
              currentIteration,
              getVersion(),
              seed
      );

      try {
        caseInstance.save();
      } catch (OWLOntologyStorageException | IOException | OWLOntologyCreationException e) {
        logger.error("Failed to information file with exception {}.", e.getMessage());
        return false;
      }
    }

    for (CatalystBlock catalystBlock : changeSpecification.getCatalysts()) {
      CaseInstance caseInstance = new CaseInstance(
          runName,
          catalystBlock.getName(),
          catalystBlock.getConstraints().enforceCoherence().orElse(null),
          catalystBlock.getConstraints().enforceConsistency().orElse(null),
          requiredEntailed(catalystBlock.getConstraints(), match),
          requiredNotEntailed(catalystBlock.getConstraints(), match),
          newOntology,
          Paths.get(sourcePath),
          changeSpecification.getHeaderBlock().getName(),
          outputDir,
          distinctnessPolicy.getMultiplicity(),
          currentIteration,
          getVersion(),
          seed
      );

      try {
        caseInstance.save();
      } catch (OWLOntologyStorageException | IOException | OWLOntologyCreationException e) {
        logger.error("Failed to save catalyst {} with exception {}.", catalystBlock.getName(), e);
        return false;
      }
    }
    return true;
  }

  private static Set<OWLLogicalAxiom> requiredEntailed(IntegrityConstraints integrityConstraints,
      Match match) {
    if (!integrityConstraints.getAxiomRequirements().isPresent()) {
      return Collections.emptySet();
    }
    return integrityConstraints.getAxiomRequirements().get().stream()
        .filter(req -> req.isEntailed().orElse(Boolean.FALSE))
        .map(AxiomRequirement::getAxiomSpecification)
        .map(spec -> AxiomSpecificationToOWLAxiomTranslator.translate(spec, match))
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toSet());
  }

  private static Set<OWLLogicalAxiom> requiredNotEntailed(IntegrityConstraints integrityConstraints,
      Match match) {
    if (!integrityConstraints.getAxiomRequirements().isPresent()) {
      return Collections.emptySet();
    }
    return integrityConstraints.getAxiomRequirements().get().stream()
        .filter(req -> !req.isEntailed().orElse(Boolean.TRUE))
        .map(AxiomRequirement::getAxiomSpecification)
        .map(spec -> AxiomSpecificationToOWLAxiomTranslator.translate(spec, match))
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toSet());
  }

  private static void saveModifiedOntology(OWLOntology modifiedOntology, String name,
      Path outputDir, int currentIteration) throws IOException, OWLOntologyStorageException {
    String filename = name + "_" + currentIteration + "_onto.owl.xml";
    Path outputPath = Paths.get(outputDir.toString(), filename);

    try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8)) {
      OWLOntologyDocumentTarget documentTarget = new WriterDocumentTarget(writer);
      modifiedOntology.saveOntology(new OWLXMLDocumentFormat(), documentTarget);
    }
  }

  private static String getVersion() throws IOException {
    try (InputStream input = ChangeCaseCreator.class.getClassLoader()
        .getResourceAsStream("config.properties")) {

      if (input == null) {
        throw new IOException();
      }
      Properties prop = new Properties();

      prop.load(input);

      return prop.getProperty("version");

    }
  }

  static class MatchSearchResult {

    private Match match;
    private OWLOntology newOntology;

    public MatchSearchResult(Match match, OWLOntology newOntology) {
      this.match = match;
      this.newOntology = newOntology;
    }

    public Optional<Match> getMatch() {
      return Optional.ofNullable(match);
    }

    public Optional<OWLOntology> getNewOntology() {
      return Optional.ofNullable(newOntology);
    }
  }
}
