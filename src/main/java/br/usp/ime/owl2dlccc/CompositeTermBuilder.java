/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.OWL2DLConstructor;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectIntersectionOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectUnionOfImpl;

public class CompositeTermBuilder {

  private static final Logger logger = LoggerFactory.getLogger(CompositeTermBuilder.class);

  private final Match primitivesMatch;

  public CompositeTermBuilder(Match primitivesMatch) {
    this.primitivesMatch = primitivesMatch;
  }

  public Optional<OWLClassExpression> buildClassExpression(
      CompositeTerm compositeTerm) {
    OWL2DLConstructor constructor = compositeTerm.getConstructor();

    Set<String> invalidOperands = compositeTerm.getOperands().stream()
        .filter(name -> !primitivesMatch.getEntityType(name).isPresent())
        .collect(Collectors.toSet());

    if (!invalidOperands.isEmpty()) {
      logger.error(
          "Composite term declaration uses undeclared or non-primitive operands: {}\nSkipping...",
          invalidOperands);
    }

    if (constructor.equals(OWL2DLConstructor.DISJUNCTION)) {
      return Optional.of(buildDisjunction(compositeTerm));
    } else if (constructor.equals(OWL2DLConstructor.CONJUNCTION)) {
      return Optional.of(buildConjunction(compositeTerm));
    }

    return Optional.empty();
  }

  private OWLObjectIntersectionOf buildConjunction(
      CompositeTerm compositeTerm) {
    Stream<OWLClassExpression> owlClassExpressionStream = compositeTerm.getOperands().stream()
        .map(primitivesMatch::getClassExpression)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty));
    List<OWLClassExpression> collect = owlClassExpressionStream.collect(Collectors.toList());
    return new OWLObjectIntersectionOfImpl(collect);
  }

  private OWLObjectUnionOf buildDisjunction(CompositeTerm compositeTerm) {
    Stream<OWLClassExpression> owlClassExpressionStream = compositeTerm.getOperands().stream()
        .map(primitivesMatch::getClassExpression)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty));
    return new OWLObjectUnionOfImpl(owlClassExpressionStream.collect(Collectors.toList()));
  }
}
