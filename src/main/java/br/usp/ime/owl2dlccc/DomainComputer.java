/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.graph.Network;

import br.usp.ime.owl2dlccc.axiomreqtofilters.EntityDomainFilter;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import uk.ac.manchester.cs.owl.owlapi.OWLEquivalentClassesAxiomImpl;

public class DomainComputer {

  private static final Logger logger = LoggerFactory.getLogger(DomainComputer.class);

  private static final String TESTING_IRI = "http://this.is.a.test.IRI/owl2dlccc/";

  private final OWLOntology ontology;
  private final Network<String, AxiomRequirement> requirementNetwork;
  private final EntityDomainFilter filterer;
  private final OWLDataFactory dataFactory;
  private final PrefixManager prefixManager;
  private final BiMap<OWLClass, OWLClassExpression> extensionBiMap;
  private List<OWLClassExpression> conceptNames = null;
  private List<OWLClassExpression> componentsExpressions = null;
  private int newClasses = 0;

  public DomainComputer(
      OWLOntology ontology,
      OWLReasonerFactory reasonerFactory,
      AxiomRequirementNetwork requirementNetwork)
      throws OWLOntologyCreationException {
    this.ontology = ontology;
    this.requirementNetwork = requirementNetwork.getRequirementNetwork();
    this.dataFactory = ontology.getOWLOntologyManager().getOWLDataFactory();
    IRI iri = IRI.create(TESTING_IRI);
    prefixManager = new DefaultPrefixManager(iri.toString());
    extensionBiMap = HashBiMap.create();
    OWLOntology extendedOntology = extendedOntology(this.ontology);

    OWLReasoner extendedReasoner = reasonerFactory.createNonBufferingReasoner(extendedOntology);

    this.filterer =
        new EntityDomainFilter(ontology, extendedReasoner, ImmutableBiMap.copyOf(extensionBiMap));
  }

  /**
   * Recursively flattens the input object if it is a collection or a Stream.
   * It only allows the objects contained to be {@link OWLClassExpression}, 
   * {@link OWLObjectPropertyExpression}, or {@link OWLIndividual}.
   * Returns an empty stream if the input does not fit any of the types mentioned above.
   * Note that it does not recursively decomposes complex expressions into simpler ones.
   * @param x the object to be "flatted" into a stream
   * @return a fully flattened stream of objects
   */
  Stream<Object> recFlat(Object x) {
    if (x instanceof Collection) {
      return ((Collection<?>) x)
          .stream().map(this::recFlat).reduce(Stream::concat).orElse(Stream.empty());
    } else if (x instanceof Stream) {
      return ((Stream<?>) x).map(this::recFlat).reduce(Stream::concat).orElse(Stream.empty());
    } else if (x instanceof OWLClassExpression
        || x instanceof OWLObjectPropertyExpression
        || x instanceof OWLIndividual) {
      return Stream.of(x);
    }
    return Stream.empty();
  }

  private OWLOntology extendedOntology(OWLOntology source) throws OWLOntologyCreationException {
    Stream<OWLClassExpression> complexClassExpressions =
        source
            .logicalAxioms()
            .flatMap(ax -> recFlat(ax.componentsWithoutAnnotations()))
            .filter(x -> x instanceof OWLClassExpression)
            .distinct()
            .map(x -> (OWLClassExpression) x)
            .filter(ce -> !ce.getClassExpressionType().equals(ClassExpressionType.OWL_CLASS));

    OWLOntologyManager owlOntologyManager = ontology.getOWLOntologyManager();
    OWLOntology extended = owlOntologyManager.createOntology(source.axioms());
    complexClassExpressions.forEach(ce -> computeExtension(extended, ce));
    return extended;
  }

  // Create new class
  // Create new OWLClassAxiom
  // Add the extension to the map
  // Add the equivalent axiom to the ontology
  private void computeExtension(OWLOntology extendedOntology, OWLClassExpression classExpression) {
    OWLClass newOWLClass;
    if (!extensionBiMap.containsValue(classExpression)) {
      newOWLClass = getNewClass(extendedOntology);
      extensionBiMap.put(newOWLClass, classExpression);
    } else {
      newOWLClass = extensionBiMap.inverse().get(classExpression);
    }
    OWLEquivalentClassesAxiom termDefinitionAxiom =
        new OWLEquivalentClassesAxiomImpl(
            Lists.newArrayList(newOWLClass, classExpression), Collections.emptySet());
    extendedOntology.add(termDefinitionAxiom);
  }

  private OWLClass getNewClass(OWLOntology ontology) {
    String newName =
        NewNameGenerator.generateNewName(ontology, this.prefixManager.getDefaultPrefix());
    if (newName == null) {
      throw new RuntimeException("Failed to obtain a fresh name");
    }
    return this.dataFactory.getOWLClass(
        ":" + newName + String.valueOf(++newClasses), this.prefixManager);
  }

  private Optional<OWLClass> getFreshConceptName() {
    return Optional.ofNullable(getNewClass(this.ontology));
  }

  Set<OWLClassExpression> computeClassDomain(PrimitiveTerm term, Match match) {
    Set<OWLClassExpression> domain = getClassExpressions(term).collect(Collectors.toSet());

    /*Set<OWLClassExpression> toRemove = domain.stream().filter(ce -> !ce.isOWLClass())
        .collect(Collectors.toSet());

    domain.removeAll(toRemove);
    domain.addAll(toRemove.stream().map(ce -> extensionBiMap.inverse().get(ce)).collect(Collectors.toSet()));*/

    if (!requirementNetwork.nodes().contains(term.getName())) {
      logger.warn("Class term {} not used in requirements.", term.getName());
      return new HashSet<>(domain);
    }

    for (AxiomRequirement requirement : requirementNetwork.incidentEdges(term.getName())) {
      filterer.cutClassDomain(domain, requirement, term, match);
      if (domain.isEmpty()) {
        return Collections.emptySet();
      }
    }
    return domain;
  }

  Set<OWLObjectPropertyExpression> computeObjectPropertyDomain(PrimitiveTerm term, Match match) {

    Set<OWLObjectPropertyExpression> domain =
        getObjectPropertyExpressions(term).collect(Collectors.toSet());

    if (!requirementNetwork.nodes().contains(term.getName())) {
      logger.warn("Object property term {} not used in requirements.", term.getName());
      return domain;
    }

    for (AxiomRequirement requirement : requirementNetwork.incidentEdges(term.getName())) {
      filterer.cutObjectPropertyDomain(domain, requirement, term, match);
      if (domain.isEmpty()) {
        return Collections.emptySet();
      }
    }
    return domain;
  }

  Set<OWLDataPropertyExpression> computeDataPropertyDomain(PrimitiveTerm term, Match match) {
    Set<OWLDataPropertyExpression> domain =
        getDataPropertyExpressions(term).collect(Collectors.toSet());

    if (!requirementNetwork.nodes().contains(term.getName())) {
      logger.warn("Data property term {} not used in requirements.", term.getName());
      return domain;
    }

    for (AxiomRequirement requirement : requirementNetwork.incidentEdges(term.getName())) {
      filterer.cutDataPropertyDomain(domain, requirement, term, match);
      if (domain.isEmpty()) {
        return Collections.emptySet();
      }
    }
    return domain;
  }

  Set<OWLIndividual> computeIndividualDomain(PrimitiveTerm term, Match match) {
    Set<OWLIndividual> domain = getOWLIndividuals(term).collect(Collectors.toSet());

    if (!requirementNetwork.nodes().contains(term.getName())) {
      logger.warn("Individual term {} not used in requirements.", term.getName());
      return domain;
    }
    for (AxiomRequirement requirement : requirementNetwork.incidentEdges(term.getName())) {
      filterer.cutIndividualDomain(domain, requirement, term, match);
      if (domain.isEmpty()) {
        return Collections.emptySet();
      }
    }
    return domain;
  }

  private OWLClassExpression getOWLThing() {
    return this.dataFactory.getOWLThing();
  }

  private OWLClassExpression getOWLNothing() {
    return this.dataFactory.getOWLNothing();
  }

  private Optional<OWLObjectPropertyExpression> getFreshRoleName() {
    return Optional.empty();
  }

  private Optional<OWLClassExpression> getUsedConceptName() {
    Optional<OWLClass> optClass = this.ontology.classesInSignature().findAny();
    OWLClassExpression clsExp = optClass.orElse(null);
    return Optional.ofNullable(clsExp);
  }

  Optional<OWLIndividual> getFreshIndividual() {
    return Optional.ofNullable(this.dataFactory.getOWLAnonymousIndividual());
  }

  private Stream<OWLClassExpression> getClassExpressions(PrimitiveTerm primitiveTerm) {

    if (componentsExpressions == null) {
      componentsExpressions =
          this.ontology
              .logicalAxioms()
              .flatMap(ax -> recFlat(ax.componentsWithoutAnnotations()))
              .distinct()
              .filter(x -> x instanceof OWLClassExpression)
              .map(x -> (OWLClassExpression) x)
              .collect(Collectors.toList());
    }

    if (conceptNames == null) {
      conceptNames = ontology.classesInSignature().collect(Collectors.toList());
    }

    Optional<Boolean> optThing = primitiveTerm.getOWLThing();
    Optional<Boolean> optNothing = primitiveTerm.getOWLNothing();
    Optional<Boolean> optComplex = primitiveTerm.getComplex();
    Optional<Boolean> optUsed = primitiveTerm.getUsed();

    boolean mustOWLThing = optThing.isPresent() && (optThing.get().equals(Boolean.TRUE));
    boolean mustNotOWLThing = optThing.isPresent() && (optThing.get().equals(Boolean.FALSE));

    boolean mustOWLNothing = optNothing.isPresent() && (optNothing.get().equals(Boolean.TRUE));
    boolean mustNotOWLNothing = optNothing.isPresent() && (optNothing.get().equals(Boolean.FALSE));

    boolean mustComplex = optComplex.isPresent() && (optComplex.get().equals(Boolean.TRUE));
    boolean mustConceptName = optComplex.isPresent() && (optComplex.get().equals(Boolean.FALSE));

    boolean mustFresh = optUsed.isPresent() && (optUsed.get().equals(Boolean.FALSE));

    // The first step is to handle requisitions for OWLThing and OWLNothing

    Stream<OWLClassExpression> baseStream = null;

    if (mustOWLThing) {
      if (mustOWLNothing) {
        return Stream.of(getOWLThing(), getOWLNothing());
      } else {
        return Stream.of(getOWLThing());
      }
    } else if (mustNotOWLThing) {
      if (mustOWLNothing) {
        return Stream.of(getOWLNothing());
      } else if (mustNotOWLNothing) {
        baseStream =
            Stream.concat(conceptNames.stream(), componentsExpressions.stream())
                .filter(ce -> !(ce.isOWLNothing() || ce.isOWLThing()));
      } else {
        baseStream =
            Stream.concat(conceptNames.stream(), componentsExpressions.stream())
                .filter(ce -> !ce.isOWLThing());
      }
    } else {
      if (mustOWLNothing) {
        return Stream.of(getOWLNothing());
      } else if (mustNotOWLNothing) {
        baseStream =
            Stream.concat(conceptNames.stream(), componentsExpressions.stream())
                .filter(ce -> !ce.isOWLNothing());
      }
    }

    if (mustComplex) {
      if (!mustFresh) {
        baseStream =
            componentsExpressions.stream()
                .filter(ce -> !ce.getClassExpressionType().equals(ClassExpressionType.OWL_CLASS));
      } else {
        logger.warn("Invalid combination: fresh and complex for {}.", primitiveTerm.getName());
        return Stream.empty();
      }
    } else if (mustConceptName) {
      if (!mustFresh) {
        baseStream =
            conceptNames.stream()
                .filter(ce -> ce.getClassExpressionType().equals(ClassExpressionType.OWL_CLASS));
      } else {
        return Stream.of(getFreshConceptName().orElse(null));
      }
    } else if (mustFresh) {
      return Stream.of(getFreshConceptName().orElse(null));
    }

    return (baseStream == null)
        ? Stream.concat(conceptNames.stream(), componentsExpressions.stream()).distinct()
        : baseStream.distinct();
  }

  private Stream<OWLObjectPropertyExpression> getObjectPropertyExpressions(
      PrimitiveTerm primitiveTerm) {

    Optional<Boolean> optUsed = primitiveTerm.getUsed();

    boolean mustFresh = optUsed.isPresent() && (optUsed.get().equals(Boolean.FALSE));

    if (mustFresh) {
      return Stream.of(getFreshRoleName().orElse(null));
    }
    return this.ontology
        .objectPropertiesInSignature()
        .map(OWLPropertyExpression::asObjectPropertyExpression);
  }

  // TODO: improve
  private Stream<OWLDataPropertyExpression> getDataPropertyExpressions(
      PrimitiveTerm primitiveTerm) {

    return this.ontology
        .dataPropertiesInSignature()
        .map(OWLPropertyExpression::asDataPropertyExpression);
  }

  public Stream<OWLIndividual> getOWLIndividuals(PrimitiveTerm primitiveTerm) {
    Optional<Boolean> optUsed = primitiveTerm.getUsed();
    boolean mustFresh = optUsed.isPresent() && (optUsed.get().equals(Boolean.FALSE));

    if (mustFresh) {
      return Stream.of(getFreshIndividual().orElse(null));
    }
    return this.ontology.individualsInSignature().map(individual -> individual);
  }
}
