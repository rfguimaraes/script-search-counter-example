/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.parsing.ChangeSpecificationParser;
import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.common.collect.Sets;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import org.apache.commons.lang3.RandomStringUtils;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

public class App {

  @Parameter(names = {"-h", "--help"}, help = true)
  boolean help;

  @Parameter(names = {"--source",
      "-s"}, required = true, description = "Path to the source ontology file")
  String sourceOntologyPath;

  @Parameter(names = {"--case",
      "-c"}, required = true, description = "Path to the case description JSON file")
  String patternPath;

  @Parameter(names = {"--outputDirectory",
      "-d"}, required = true, description = "Path to the output directory")
  String outputPath;

  @Parameter(names = {"--reasoner",
      "-r"}, validateWith = ReasonerValidator.class, description = "Reasoner choice: HermiT or JFact")
  String reasoner = "HermiT";

  @Parameter(names = {"--name",
      "-n"}, description = "Base name to use when generating the output files")
  String name;

  @Parameter(names = {"-m",
      "--multiplicity"}, validateWith = PositiveIntegerValidator.class, description = "How many times the terms, conditions and actions should be multiplied")
  Integer multiplicity = 1;

  @Parameter(names = {"-i",
      "--iterations"}, validateWith = PositiveIntegerValidator.class, description = "How many sequential cases the generator should (attempt to) produce using the same input")
  Integer iterations = 1;

  @Parameter(names = {"-g",
      "--seed"}, description = "A seed (long integer) to feed a Pseudo-RNG. If none provided the program will choose one")
  Long seed;

  @Parameter(names = {"-t",
      "--timeout"}, validateWith = PositiveLongOrNullValidator.class, description = "The time limit in seconds for running a single iteration. There will be no timeout if none is given.")
  Long timeout;

  public static void main(String... argv)
      throws OWLOntologyCreationException, IOException, SpecificationValidationException, OWLOntologyStorageException {
    App app = new App();
    JCommander jCommanderApp = JCommander.newBuilder()
        .addObject(app)
        .build();

    jCommanderApp.parse(argv);
    if (app.help) {
      jCommanderApp.usage();
      return;
    }
    app.run();
  }

  public void run()
      throws IOException, OWLOntologyCreationException, OWLOntologyStorageException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology sourceOntology = manager
        .loadOntologyFromOntologyDocument(Files.newInputStream(Paths.get(sourceOntologyPath)));

    ChangeSpecification changeSpecification;

    try (BufferedReader reader = Files.newBufferedReader(Paths.get(patternPath))) {
      changeSpecification = ChangeSpecificationParser
          .parseChangeSpecification(reader);
    }

    if (multiplicity > 1) {
      changeSpecification = SpecificationExpander.expand(changeSpecification, multiplicity);
    }

    OWLReasonerFactory reasonerFactory = getReasonerFactory(reasoner)
        .orElseThrow(RuntimeException::new);

    if (name == null || name.equals("")) {
      name = RandomStringUtils.randomAlphanumeric(15);
    }

    if (seed == null) {
      seed = new Random().nextLong();
    }

    boolean success = ChangeCaseCreator
        .apply(sourceOntology, sourceOntologyPath, changeSpecification, reasonerFactory,
            Paths.get(outputPath), name, new DistinctnessPolicy(multiplicity), iterations, seed,
            (timeout == null || timeout <= 0) ? null : timeout * 1000);

    if (success) {
      System.out.println("Success!");
    } else {
      System.out.println("Could not \"break\" the ontology properly.");
    }
  }

  public static class PositiveLongOrNullValidator implements IParameterValidator {

    public void validate(String name, String value)
        throws ParameterException {
      if (value == null) {
        return;
      }
      long n = Long.parseLong(value);
      if (n < 0) {
        throw new ParameterException(
            "Parameter " + name + " should be positive or null (found " + value + ")");
      }
    }
  }

  public static class ReasonerValidator implements IParameterValidator {

    public final Set<String> validReasoners = Sets.newHashSet("HermiT", "JFact");

    @Override
    public void validate(String name, String value) throws ParameterException {
      if (validReasoners.stream().noneMatch(value::equalsIgnoreCase)) {
        throw new ParameterException("Invalid reasoner: " + value + "(passed with " + name + ")");
      }
    }
  }

  public static class PositiveIntegerValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
      int n = Integer.parseInt(value);
      if (n < 1) {
        throw new ParameterException(
            "Parameter " + name + " should be at least 1 (found " + value + ")");
      }
    }
  }

  public static Optional<OWLReasonerFactory> getReasonerFactory(String reasonerName) {
    if (reasonerName.equalsIgnoreCase("HermiT")) {
      return Optional.of(new ReasonerFactory());
    } else if (reasonerName.equalsIgnoreCase("JFact")) {
      return Optional.of(new JFactFactory());
    }

    return Optional.empty();
  }
}
