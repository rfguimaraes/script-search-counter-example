/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.graph.ElementOrder;
import com.google.common.graph.ImmutableNetwork;
import com.google.common.graph.MutableNetwork;
import com.google.common.graph.NetworkBuilder;
import java.util.ArrayList;
import java.util.List;

public class AxiomRequirementNetwork {

  private final List<AxiomRequirement> requirements;
  private final List<PrimitiveTerm> primitives;
  private final ImmutableNetwork<String, AxiomRequirement> requirementNetwork;

  public AxiomRequirementNetwork(
      List<AxiomRequirement> requirements,
      List<PrimitiveTerm> primitives) {
    this.requirements = requirements;
    this.primitives = primitives;
    requirementNetwork = buildNetwork();
  }

  private ImmutableNetwork<String, AxiomRequirement> buildNetwork() {
    MutableNetwork<String, AxiomRequirement> tempNetwork = NetworkBuilder.undirected()
        .allowsParallelEdges(true)
        .allowsSelfLoops(true)
        .nodeOrder(ElementOrder.unordered())
        .expectedNodeCount(primitives.size())
        .expectedEdgeCount(requirements.size())
        .build();
    requirements.forEach(req -> this.addNode(tempNetwork, req));
    return ImmutableNetwork.copyOf(tempNetwork);
  }

  public ImmutableNetwork<String, AxiomRequirement> getRequirementNetwork() {
    return requirementNetwork;
  }

  private void addNode(MutableNetwork<String, AxiomRequirement> network,
      AxiomRequirement requirement) {
    List<String> arguments = new ArrayList<>(requirement.getAxiomSpecification().getArguments());
    arguments.sort(String::compareTo);

    if (arguments.isEmpty()) {
      // TODO: log warning
      return;
    }

    if (arguments.size() == 1) {
      network.addEdge(
          arguments.get(0),
          arguments.get(0),
          requirement
      );
      return;
    }

    for (int i = 0; i < arguments.size() - 1; i++) {
      for (int j = i + 1; j < arguments.size(); j++) {
        network.addEdge(
            arguments.get(i),
            arguments.get(j),
            requirement
        );
      }
    }
  }
}
