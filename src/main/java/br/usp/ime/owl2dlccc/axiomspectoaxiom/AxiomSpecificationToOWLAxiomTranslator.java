/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomspectoaxiom;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.Optional;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;

public class AxiomSpecificationToOWLAxiomTranslator {

  static final Map<AxiomType, AxiomSpecificationToAxiomTranslator<? extends OWLLogicalAxiom>> translatorMap = ImmutableMap.<AxiomType, AxiomSpecificationToAxiomTranslator<? extends OWLLogicalAxiom>>builder()
      .put(AxiomType.SAME_INDIVIDUAL, new AxiomSpecificationToSameIndividualTranslator())
      .put(AxiomType.DIFFERENT_INDIVIDUALS,
          new AxiomSpecificationToDifferentIndividualsTranslator())
      .put(AxiomType.HAS_KEY, new AxiomSpecificationToHasKeyTranslator())
      .put(AxiomType.SUBCLASS_OF, new AxiomSpecificationToSubClassOfTranslator())
      .put(AxiomType.EQUIVALENT_CLASSES, new AxiomSpecificationToEquivalentClassesTranslator())
      .put(AxiomType.DISJOINT_CLASSES, new AxiomSpecificationToDisjointClassesTranslator())
      .put(AxiomType.DISJOINT_UNION, new AxiomSpecificationToDisjointUnionTranslator())
      .put(AxiomType.OBJECT_PROPERTY_DOMAIN,
          new AxiomSpecificationToObjectPropertyDomainTranslator())
      .put(AxiomType.DATA_PROPERTY_DOMAIN, new AxiomSpecificationToDataPropertyDomainTranslator())
      .put(AxiomType.OBJECT_PROPERTY_RANGE, new AxiomSpecificationToObjectPropertyRangeTranslator())
      .put(AxiomType.FUNCTIONAL_OBJECT_PROPERTY,
          new AxiomSpecificationToFunctionalObjectPropertyTranslator())
      .put(AxiomType.FUNCTIONAL_DATA_PROPERTY,
          new AxiomSpecificationToFunctionalDataPropertyTranslator())
      .put(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY,
          new AxiomSpecificationToInverseFunctionalObjectPropertyTranslator())
      .put(AxiomType.REFLEXIVE_OBJECT_PROPERTY,
          new AxiomSpecificationToReflexiveObjectPropertyTranslator())
      .put(AxiomType.IRREFLEXIVE_OBJECT_PROPERTY,
          new AxiomSpecificationToIrreflexiveObjectPropertyTranslator())
      .put(AxiomType.TRANSITIVE_OBJECT_PROPERTY,
          new AxiomSpecificationToTransitiveObjectPropertyTranslator())
      .put(AxiomType.SYMMETRIC_OBJECT_PROPERTY,
          new AxiomSpecificationToSymmetricObjectPropertyTranslator())
      .put(AxiomType.ASYMMETRIC_OBJECT_PROPERTY,
          new AxiomSpecificationToAsymmetricObjectPropertyTranslator())
      .put(AxiomType.SUB_OBJECT_PROPERTY, new AxiomSpecificationToSubObjectPropertyOfTranslator())
      .put(AxiomType.SUB_DATA_PROPERTY, new AxiomSpecificationToSubDataPropertyOfTranslator())
      .put(AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
          new AxiomSpecificationToEquivalentObjectPropertiesTranslator())
      .put(AxiomType.EQUIVALENT_DATA_PROPERTIES,
          new AxiomSpecificationToEquivalentDataPropertiesTranslator())
      .put(AxiomType.SUB_PROPERTY_CHAIN_OF, new AxiomSpecificationToSubPropertyChainOfTranslator())
      .put(AxiomType.INVERSE_OBJECT_PROPERTIES,
          new AxiomSpecificationToInverseObjectPropertiesTranslator())
      .put(AxiomType.DISJOINT_OBJECT_PROPERTIES,
          new AxiomSpecificationToDisjointObjectPropertiesTranslator())
      .put(AxiomType.DISJOINT_DATA_PROPERTIES,
          new AxiomSpecificationToDisjointDataPropertiesTranslator())
      .put(AxiomType.CLASS_ASSERTION, new AxiomSpecificationToClassAssertionTranslator())
      .build();

  public static Optional<? extends OWLLogicalAxiom> translate(AxiomSpecification axiomSpecification,
      Match match) {

    AxiomType axiomType = axiomSpecification.getAxiomType();

    if (translatorMap.containsKey(axiomType)) {
      return translatorMap.get(axiomType).translate(axiomSpecification, match);
    }

    // TODO: warning for unsupported axiom type
    return Optional.empty();
  }
}
