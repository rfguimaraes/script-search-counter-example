/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomspectoaxiom;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AxiomSpecificationToAxiomTranslator<T extends OWLLogicalAxiom> {

  private static final Logger logger = LoggerFactory
      .getLogger(AxiomSpecificationToAxiomTranslator.class);

  Optional<T> translate(AxiomSpecification axiomSpecification, Match match) {

    Set<String> undeclared = axiomSpecification.getArguments().stream()
        .filter(name -> !match.getEntityType(name).isPresent()).collect(
            Collectors.toSet());

    if (!undeclared.isEmpty()) {
      logger.error("Specification contains undeclared variables: {}\tSkipping...", undeclared);
      return Optional.empty();
    }
    return translateInternal(axiomSpecification, match);
  }

  abstract Optional<T> translateInternal(AxiomSpecification axiomSpecification, Match match);
}
