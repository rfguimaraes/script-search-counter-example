/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomspectoaxiom;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import uk.ac.manchester.cs.owl.owlapi.OWLSubObjectPropertyOfAxiomImpl;

class AxiomSpecificationToSubObjectPropertyOfTranslator extends
    AxiomSpecificationToAxiomTranslator<OWLSubObjectPropertyOfAxiom> {

  @Override
  Optional<OWLSubObjectPropertyOfAxiom> translateInternal(
      AxiomSpecification axiomSpecification, Match match) {
    List<String> arguments = axiomSpecification.getArguments();
    Optional<OWLObjectPropertyExpression> subProperty = match
        .getObjectPropertyExpression(arguments.get(0));
    Optional<OWLObjectPropertyExpression> superProperty = match
        .getObjectPropertyExpression(arguments.get(1));

    if (!(subProperty.isPresent() && superProperty.isPresent())) {
      return Optional.empty();
    }

    return Optional.of(new OWLSubObjectPropertyOfAxiomImpl(subProperty.get(), superProperty.get(),
        Collections.emptySet()));
  }
}
