/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomspectoaxiom;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import uk.ac.manchester.cs.owl.owlapi.OWLDifferentIndividualsAxiomImpl;

class AxiomSpecificationToDifferentIndividualsTranslator extends
    AxiomSpecificationToAxiomTranslator<OWLDifferentIndividualsAxiom> {

  @Override
  Optional<OWLDifferentIndividualsAxiom> translateInternal(AxiomSpecification axiomSpecification,
      Match match) {
    List<OWLIndividual> individuals = axiomSpecification.getArguments().stream()
        .map(match::getIndividual)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());

    return Optional.ofNullable(individuals.size() < 2 ? null
        : new OWLDifferentIndividualsAxiomImpl(individuals, Collections.emptyList()));
  }
}
