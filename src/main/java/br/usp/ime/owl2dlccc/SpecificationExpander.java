/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.ActionBlock;
import br.usp.ime.owl2dlccc.model.ActionBlockImpl;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.AxiomRequirementImpl;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import br.usp.ime.owl2dlccc.model.AxiomSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CatalystBlock;
import br.usp.ime.owl2dlccc.model.CatalystBlockImpl;
import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.model.ChangeSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.CompositeTermImpl;
import br.usp.ime.owl2dlccc.model.IntegrityConstraints;
import br.usp.ime.owl2dlccc.model.IntegrityConstraintsImpl;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import br.usp.ime.owl2dlccc.model.PrimitiveTermImpl;
import br.usp.ime.owl2dlccc.model.TermsBlock;
import br.usp.ime.owl2dlccc.model.TermsBlockImpl;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SpecificationExpander {

  public static Stream<PrimitiveTerm> expand(
      PrimitiveTerm primitiveTerm, int n) {
    return IntStream.range(0, n).mapToObj(i -> new PrimitiveTermImpl(
        primitiveTerm.getName() + "_" + i,
        primitiveTerm.getEntityType(),
        primitiveTerm.getUsed().orElse(null),
        primitiveTerm.getComplex().orElse(null),
        primitiveTerm.getOWLThing().orElse(null),
        primitiveTerm.getOWLNothing().orElse(null)
    ));
  }

  public static Stream<CompositeTerm> expand(
      CompositeTerm compositeTerm, int n) {
    return IntStream.range(0, n).mapToObj(i -> new CompositeTermImpl(
        compositeTerm.getName() + "_" + i,
        compositeTerm.getEntityType(),
        compositeTerm.getConstructor(),
        compositeTerm.getOperands().stream().map(operand -> operand + "_" + i).collect(
            Collectors.toList())
    ));
  }

  public static List<CompositeTerm> expandC(
      List<CompositeTerm> composites, int n) {
    return composites.stream().flatMap(term -> expand(term, n))
        .collect(Collectors.toList());
  }

  public static List<PrimitiveTerm> expandP(
      List<PrimitiveTerm> primitives, int n) {
    return primitives.stream().flatMap(term -> expand(term, n))
        .collect(Collectors.toList());
  }

  public static TermsBlock expand(TermsBlock termsBlock, int n) {

    TermsBlock expandedTermBlock = new TermsBlockImpl();

    if (termsBlock.getPrimitives().isPresent()) {
      expandedTermBlock.setPrimitives(expandP(termsBlock.getPrimitives().get(), n));
    } else {
      termsBlock.setPrimitives(null);
    }

    if (termsBlock.getComposites().isPresent()) {
      expandedTermBlock.setComposites(expandC(termsBlock.getComposites().get(), n));
    } else {
      termsBlock.setPrimitives(null);
    }

    return expandedTermBlock;
  }

  public static Stream<AxiomSpecification> expand(AxiomSpecification axiomSpecification, int n) {
    return IntStream.range(0, n).mapToObj(i -> extend(axiomSpecification, i));
  }

  public static AxiomSpecification extend(AxiomSpecification axiomSpecification, int i) {
    return new AxiomSpecificationImpl(
        axiomSpecification.getAxiomType(),
        axiomSpecification.getArguments().stream().map(arg -> arg + "_" + i)
            .collect(Collectors.toList())
    );
  }

  public static Stream<AxiomRequirement> expand(AxiomRequirement requirement, int n) {
    return IntStream.range(0, n).mapToObj(i -> new AxiomRequirementImpl(
        extend(requirement.getAxiomSpecification(), i),
        requirement.isAsserted().orElse(null),
        requirement.isEntailed().orElse(null)
    ));
  }

  public static List<AxiomRequirement> expand(List<AxiomRequirement> requirements, int n) {
    return requirements.stream().flatMap(requirement -> expand(requirement, n))
        .collect(Collectors.toList());
  }

  public static IntegrityConstraints expand(IntegrityConstraints constraints, int n) {
    return new IntegrityConstraintsImpl(
        constraints.enforceCoherence().orElse(null),
        constraints.enforceConsistency().orElse(null),
        constraints.getAxiomRequirements().isPresent() ? expand(
            constraints.getAxiomRequirements().get(), n) : Collections.emptyList()
    );
  }

  public static ActionBlock expand(ActionBlock actionBlock, int n) {
    List<AxiomSpecification> expandedAdditions = actionBlock.getAdditions().map(
        additions -> additions.stream().flatMap(
            spec -> expand(spec, n)
        ).collect(Collectors.toList())
    ).orElse(Collections.emptyList());

    List<AxiomSpecification> expandedRemovals = actionBlock.getRemovals().map(
        removals -> removals.stream().flatMap(
            spec -> expand(spec, n)
        ).collect(Collectors.toList())
    ).orElse(Collections.emptyList());

    return new ActionBlockImpl(expandedAdditions, expandedRemovals);
  }

  public static CatalystBlock expand(CatalystBlock catalyst, int n) {
    return new CatalystBlockImpl(
        catalyst.getName(),
        expand(catalyst.getConstraints(), n)
    );
  }

  public static List<CatalystBlock> expandCatalysts(List<CatalystBlock> catalysts, int n) {
    return catalysts.stream().map(catalyst -> expand(catalyst, n)).collect(Collectors.toList());
  }

  public static ChangeSpecification expand(ChangeSpecification changeSpecification, int n) {

    TermsBlock expandedTermsBlock = changeSpecification.getTermsBlock().isPresent() ? expand(
        changeSpecification.getTermsBlock().get(), n) : null;

    IntegrityConstraints expandedPreconditions =
        changeSpecification.getPreconditions().isPresent() ? expand(
            changeSpecification.getPreconditions().get(), n) : null;

    ActionBlock expandedActionBlock = changeSpecification.getActionBlock().isPresent() ? expand(
        changeSpecification.getActionBlock().get(), n) : null;

    IntegrityConstraints expandedPostconditions =
        changeSpecification.getPostconditions().isPresent() ? expand(
            changeSpecification.getPostconditions().get(), n) : null;

    return new ChangeSpecificationImpl(
        changeSpecification.getHeaderBlock(),
        expandedTermsBlock,
        expandedPreconditions,
        expandedActionBlock,
        expandedPostconditions,
        expandCatalysts(changeSpecification.getCatalysts(), n)
    );
  }
}
