/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import br.usp.ime.owl2dlccc.model.Term;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.EntityType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrimitiveMatchFactory implements PrimitiveMatchIterator {

  private static final Logger logger = LoggerFactory.getLogger(PrimitiveMatchFactory.class);

  private Match nextMatch;
  private final List<PrimitiveTerm> terms;

  protected final SortedMap<String, EntityType> typeMap;
  protected final Map<String, List<OWLClassExpression>> classDomainMap;
  protected final Map<String, List<OWLObjectPropertyExpression>> objectPropertyDomainMap;
  protected final Map<String, List<OWLDataPropertyExpression>> dataPropertyDomainMap;
  protected final Map<String, List<OWLIndividual>> individualDomainMap;
  protected final DistinctnessPolicy distinctnessPolicy;
  protected final Map<String, Boolean> validDomain;
  private final DomainComputer domainComputer;
  private final Map<String, Iterator<? extends OWLObject>> iteratorMap;
  private boolean started;
  private final Match partialMatch;
  private final Random random;

  public PrimitiveMatchFactory(List<PrimitiveTerm> terms, DomainComputer domainComputer,
      DistinctnessPolicy distinctnessPolicy, Match partialMatch, long seed) {
    // Consider only the terms that are not in the partial match
    this.terms = Optional.ofNullable(partialMatch).map(pm -> terms.stream()
        .filter(key -> !pm.getEntityType(key.getName()).isPresent()).collect(
            Collectors.toList())).orElse(terms);
    random = new Random(seed);
    Collections.sort(terms);
    Collections.shuffle(terms, random);
    typeMap = new TreeMap<>();
    classDomainMap = new HashMap<>();
    objectPropertyDomainMap = new HashMap<>();
    dataPropertyDomainMap = new HashMap<>();
    individualDomainMap = new HashMap<>();
    validDomain = new HashMap<>();
    terms.forEach(term -> validDomain.put(term.getName(), false));
    this.distinctnessPolicy = distinctnessPolicy;
    this.domainComputer = domainComputer;
    iteratorMap = new HashMap<>();
    started = false;
    this.partialMatch = partialMatch;
    computeFirst();
  }

  @Override
  public boolean hasNext() {
    return nextMatch != null;
  }

  private void computeFirst() {

    if (terms.size() == 0) {
      nextMatch = partialMatch;
    } else {
      update(terms.size() - 1);
    }
  }

  private void computeNext() {
    update(0);
  }

  public void update(Set<Term> failures) {
    if (nextMatch == null) {
      return;
    }
    Optional<Integer> optMin = failures.stream().filter(term -> term instanceof PrimitiveTerm)
        .map(term -> (PrimitiveTerm) term)
        .map(terms::indexOf).filter(x -> x >= 0)
        .reduce(Integer::min);

    optMin.ifPresent(this::update);
  }

  private void update(int start) {
    // The start argument means that I want to update some position straight way instead of waiting
    // to reach there from zero. But after that, I should proceed as usual.
    Match match = new Match((nextMatch == null) ? partialMatch : nextMatch);

    boolean carry = false;
    if (start != 0) {
      carry = advance(match, start);
      if (carry && start == terms.size() - 1) {
        // Overflow
        nextMatch = null;
        return;
      } else if (match.allDistinct(this.distinctnessPolicy)) {
        // Done
        nextMatch = match;
        return;
      }
    }

    int digit = 0;
    while (digit < terms.size()) {
      carry = advance(match, digit);
      if (!carry) {
        if (match.allDistinct(this.distinctnessPolicy)) {
          nextMatch = match;
          return;
        } else {
          digit = 0;
        }
      } else {
        digit++;
      }
    }

    nextMatch = null;
  }

  @Override
  public Match next() {

    if (nextMatch != null) {
      Match toReturn = new Match(nextMatch);
      computeNext();
      return toReturn;
    }
    return null;
  }

  // Start from which digit?
  // Returns true if it needs the next digit to be cycled (advanced), false otherwise
  private boolean advance(Match match, int digit) {

    // TODO: review this
    if (!validDomain.get(terms.get(digit).getName())) {
      if (digit == terms.size() - 1) {
        if (started) {
          return true;
        }
        started = true;
      }
      computeDomain(digit, match);
      validDomain.put(terms.get(digit).getName(), true);
    }

    Iterator<? extends OWLObject> iterator = iteratorMap.get(terms.get(digit).getName());
    while (iterator.hasNext()) {
      match.setObject(terms.get(digit).getName(), iterator.next());

      if (digit > 0) {
        validDomain.put(terms.get(digit - 1).getName(), false);
        if (!advance(match, digit - 1)) {
          // Perfect, nothing was advanced. I can just return.
          return false;
        }
      } else {
        // There was nothing below anyway, just need to assign this guy
        return false;
      }
    }
    // No suitable candidate here, advance the next dimension to try again
    return true;
  }

  // TODO: do it properly
  private void computeDomain(int digit, Match match) {
    PrimitiveTerm term = terms.get(digit);
    EntityType entityType = term.getEntityType();

    // Create a copy match, but remove the terms that will be "undefined"
    Match copyMatch = new Match(match);
    for (int i = 0; i <= digit; i++) {
      copyMatch.removeMatch(terms.get(i).getName());
    }

    if (entityType.equals(EntityType.CLASS)) {
      List<OWLClassExpression> domain =
          Lists.newArrayList(domainComputer.computeClassDomain(term, copyMatch));
      Collections.sort(domain);
      Collections.shuffle(domain, random);
      classDomainMap.put(term.getName(), domain);
    } else if (entityType.equals(EntityType.OBJECT_PROPERTY)) {

      List<OWLObjectPropertyExpression> domain =
          Lists.newArrayList(domainComputer.computeObjectPropertyDomain(term, copyMatch));
      Collections.sort(domain);
      Collections.shuffle(domain, random);
      objectPropertyDomainMap
          .put(term.getName(), domain);
    } else if (entityType.equals(EntityType.DATA_PROPERTY)) {
      List<OWLDataPropertyExpression> domain =
          Lists.newArrayList(domainComputer.computeDataPropertyDomain(term, copyMatch));
      Collections.sort(domain);
      Collections.shuffle(domain, random);
      dataPropertyDomainMap
          .put(term.getName(), domain);
    } else if (entityType.equals(EntityType.NAMED_INDIVIDUAL)) {
      List<OWLIndividual> domain = Lists
          .newArrayList(domainComputer.computeIndividualDomain(term, copyMatch));
      Collections.sort(domain);
      Collections.shuffle(domain, random);
      individualDomainMap
          .put(term.getName(), domain);
    } else {
      logger.warn("Unsupported entity type: {}.", entityType);
    }

    iteratorMap.put(term.getName(), getDomain(term).iterator());
  }

  private List<? extends OWLObject> getDomain(PrimitiveTerm term) {
    EntityType entityType = term.getEntityType();
    List<? extends OWLObject> domain = null;
    if (entityType.equals(EntityType.CLASS)) {
      domain = classDomainMap.get(term.getName());
    } else if (entityType.equals(EntityType.OBJECT_PROPERTY)) {
      domain = objectPropertyDomainMap.get(term.getName());
    } else if (entityType.equals(EntityType.DATA_PROPERTY)) {
      domain = dataPropertyDomainMap.get(term.getName());
    } else if (entityType.equals(EntityType.NAMED_INDIVIDUAL)) {
      domain = individualDomainMap.get(term.getName());
    }

    return domain;
  }
}
