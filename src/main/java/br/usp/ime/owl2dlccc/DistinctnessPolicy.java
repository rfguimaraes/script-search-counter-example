/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.Term;

public class DistinctnessPolicy {

  private final int multiplicity;

  public int getMultiplicity() {
    return multiplicity;
  }

  public DistinctnessPolicy(int multiplicity) {
    this.multiplicity = multiplicity;
  }

  private String getBaseName(String varName) {
    int separatorIndex = varName.lastIndexOf("_");
    return varName.substring(0, separatorIndex);
  }

  private int getIndex(String varName) {
    int separatorIndex = varName.lastIndexOf("_");
    return Integer.valueOf(varName.substring(separatorIndex + 1));
  }

  public boolean mustBeDistinct(String varName, String otherVarName) {

    if (multiplicity <= 1) {
      return !varName.equals(otherVarName);
    }

    int varIndex = getIndex(varName);
    int otherVarIndex = getIndex(otherVarName);

    String varBaseName = getBaseName(varName);
    String otherVarBaseName = getBaseName(otherVarName);

    return (varIndex == otherVarIndex && !varBaseName.equals(otherVarBaseName)) || (
        varIndex != otherVarIndex && varBaseName.equals(otherVarBaseName));
  }

  public boolean mustBeDistinct(Term term,
      Term otherTerm) {
    return mustBeDistinct(term.getName(), otherTerm.getName());
  }

}
