/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.semanticweb.owlapi.model.EntityType;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

public class Match {

  protected final Map<String, EntityType> typeMap;
  protected final Map<String, OWLClassExpression> classExpressionMap;
  protected final Map<String, OWLObjectPropertyExpression> objectPropertyExpressionMap;
  protected final Map<String, OWLDataPropertyExpression> dataPropertyExpressionMap;
  protected final Map<String, OWLIndividual> individualMap;

  public Match() {
    this(null);
  }

  public Match(Match match) {
    this.typeMap = (match != null) ? Maps.newHashMap(match.typeMap) : new HashMap<>();
    this.classExpressionMap =
        (match != null) ? Maps.newHashMap(match.classExpressionMap) : new HashMap<>();
    this.objectPropertyExpressionMap =
        (match != null) ? Maps.newHashMap(match.objectPropertyExpressionMap) : new HashMap<>();
    this.dataPropertyExpressionMap =
        (match != null) ? Maps.newHashMap(match.dataPropertyExpressionMap) : new HashMap<>();
    this.individualMap = (match != null) ? Maps.newHashMap(match.individualMap) : new HashMap<>();
  }

  public Optional<OWLClassExpression> getClassExpression(String key) {
    return Optional.ofNullable(classExpressionMap.get(key));
  }

  public Optional<OWLObjectPropertyExpression> getObjectPropertyExpression(String key) {
    return Optional.ofNullable(objectPropertyExpressionMap.get(key));
  }

  public Optional<OWLDataPropertyExpression> getDataPropertyExpression(String key) {
    return Optional.ofNullable(dataPropertyExpressionMap.get(key));
  }

  public Optional<OWLIndividual> getIndividual(String key) {
    return Optional.ofNullable(individualMap.get(key));
  }

  public void setObject(String key, OWLObject value) {
    if (value instanceof OWLClassExpression) {
      setClassExpression(key, (OWLClassExpression) value);
    } else if (value instanceof OWLObjectPropertyExpression) {
      setObjectPropertyExpression(key, (OWLObjectPropertyExpression) value);
    } else if (value instanceof OWLDataPropertyExpression) {
      setDataPropertyExpression(key, (OWLDataPropertyExpression) value);
    } else if (value instanceof OWLIndividual) {
      setIndividual(key, (OWLIndividual) value);
    }
  }

  public void setObject(String key, OWLClassExpression value) {
    setClassExpression(key, value);
  }

  public void setObject(String key, OWLObjectPropertyExpression value) {
    setObjectPropertyExpression(key, value);
  }

  public void setObject(String key, OWLDataPropertyExpression value) {
    setDataPropertyExpression(key, value);
  }

  public void setObject(String key, OWLIndividual value) {
    setIndividual(key, value);
  }

  public void setClassExpression(String key, OWLClassExpression value) {
    removeMatch(key);
    typeMap.put(key, EntityType.CLASS);
    classExpressionMap.put(key, value);
  }

  public void setObjectPropertyExpression(String key, OWLObjectPropertyExpression value) {
    removeMatch(key);
    typeMap.put(key, EntityType.OBJECT_PROPERTY);
    objectPropertyExpressionMap.put(key, value);
  }

  public void setDataPropertyExpression(String key, OWLDataPropertyExpression value) {
    removeMatch(key);
    typeMap.put(key, EntityType.DATA_PROPERTY);
    dataPropertyExpressionMap.put(key, value);
  }

  public void setIndividual(String key, OWLIndividual value) {
    removeMatch(key);
    typeMap.put(key, EntityType.NAMED_INDIVIDUAL);
    individualMap.put(key, value);
  }

  public void removeMatch(String key) {
    Optional<EntityType> optEntityType = this.getEntityType(key);

    if (!optEntityType.isPresent()) {
      return;
    }

    if (optEntityType.get().equals(EntityType.CLASS)) {
      classExpressionMap.remove(key);
    } else if (optEntityType.get().equals(EntityType.OBJECT_PROPERTY)) {
      objectPropertyExpressionMap.remove(key);
    } else if (optEntityType.get().equals(EntityType.DATA_PROPERTY)) {
      dataPropertyExpressionMap.remove(key);
    } else if (optEntityType.get().equals(EntityType.NAMED_INDIVIDUAL)) {
      individualMap.remove(key);
    }
  }

  public Optional<EntityType> getEntityType(String key) {
    return Optional.ofNullable(typeMap.get(key));
  }

  public String toString() {

    return "Class Expressions:\n"
        + this.classExpressionMap.toString()
        + "\n"
        + "Object Property Expressions:\n"
        + this.objectPropertyExpressionMap.toString()
        + "\n"
        + "Data Property Expressions:\n"
        + this.dataPropertyExpressionMap.toString()
        + "\n"
        + "Individuals:\n"
        + this.individualMap.toString()
        + "\n";
  }

  public boolean hasKey(String key) {
    return typeMap.containsKey(key);
  }

  public boolean allDistinct(DistinctnessPolicy distinctnessPolicy) {
    return okDistinct(classExpressionMap, distinctnessPolicy)
        && okDistinct(objectPropertyExpressionMap, distinctnessPolicy)
        && okDistinct(dataPropertyExpressionMap, distinctnessPolicy)
        && okDistinct(individualMap, distinctnessPolicy);
  }

  private boolean okDistinct(Map<String, ? extends OWLObject> map,
      DistinctnessPolicy distinctnessPolicy) {
    if (map.size() < 2) {
      return true;
    }
    return Sets.combinations(map.keySet(), 2).stream().allMatch(keys -> pairDistinct(map, keys,
        distinctnessPolicy));
  }

  private boolean pairDistinct(Map<String, ? extends OWLObject> map, Set<String> keys,
      DistinctnessPolicy distinctnessPolicy) {
    Iterator<String> iterator = keys.iterator();
    String firstKey = iterator.next();
    String secondKey = iterator.next();

    boolean mustBeDistinct = distinctnessPolicy.mustBeDistinct(firstKey, secondKey);

    return !mustBeDistinct || map.get(firstKey) != map.get(secondKey);
  }

  public boolean partiallyEqual(Match other, Set<String> keys) {
    return keys.stream().allMatch(key -> this.partiallyEqual(other, key));
  }

  public boolean partiallyEqual(Match other, String key) {
    Optional<EntityType> thisType = this.getEntityType(key);
    Optional<EntityType> otherType = other.getEntityType(key);

    if (!thisType.equals(otherType)) {
      return false;
    } else if (thisType.isPresent()) {
      if (thisType.get().equals(EntityType.CLASS)) {
        return this.getClassExpression(key).equals(other.getClassExpression(key));
      } else if (thisType.get().equals(EntityType.OBJECT_PROPERTY)) {
        return this.getObjectPropertyExpression(key).equals(other.getObjectPropertyExpression(key));
      } else if (thisType.get().equals(EntityType.DATA_PROPERTY)) {
        return this.getDataPropertyExpression(key).equals(other.getDataPropertyExpression(key));
      } else if (thisType.get().equals(EntityType.NAMED_INDIVIDUAL)) {
        return this.getIndividual(key).equals(other.getIndividual(key));
      }
    }
    return false;
  }

  public Optional<OWLEntity> getEntity(String key) {
    Optional<EntityType> optEntityType = this.getEntityType(key);

    if (!optEntityType.isPresent()) {
      return Optional.empty();
    }

    OWLEntity entity = null;
    if (optEntityType.get().equals(EntityType.CLASS)) {
      OWLClassExpression classExpression = classExpressionMap.get(key);
      if (classExpression.isOWLClass()) {
        entity = classExpression.asOWLClass();
      }
    } else if (optEntityType.get().equals(EntityType.OBJECT_PROPERTY)) {
      OWLObjectPropertyExpression objectPropertyExpression = objectPropertyExpressionMap
          .get(key);
      if (objectPropertyExpression.isOWLObjectProperty()) {
        entity = objectPropertyExpression.asOWLObjectProperty();
      }
    } else if (optEntityType.get().equals(EntityType.DATA_PROPERTY)) {
      OWLDataPropertyExpression dataPropertyExpression = dataPropertyExpressionMap.get(key);
      if (dataPropertyExpression.isOWLDataProperty()) {
        entity = dataPropertyExpression.asOWLDataProperty();
      }
    } else if (optEntityType.get().equals(EntityType.NAMED_INDIVIDUAL)) {
      OWLIndividual individual = individualMap.get(key);
      if (individual.isOWLNamedIndividual()) {
        entity = individual.asOWLNamedIndividual();
      }
    }

    return Optional.ofNullable(entity);
  }


}
