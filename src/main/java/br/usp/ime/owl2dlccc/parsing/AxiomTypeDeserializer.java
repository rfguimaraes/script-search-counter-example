/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.semanticweb.owlapi.model.AxiomType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AxiomTypeDeserializer extends StdDeserializer<AxiomType> {

  private static final Logger logger = LoggerFactory.getLogger(AxiomTypeDeserializer.class);

  public AxiomTypeDeserializer() {
    this(null);
  }

  public AxiomTypeDeserializer(Class<AxiomType> axiomTypeClass) {
    super(axiomTypeClass);
  }

  @Override
  public AxiomType deserialize(JsonParser p, DeserializationContext context)
      throws IOException {
    AxiomType<?> axiomType = AxiomType.getAxiomType(p.getValueAsString());
    if (axiomType == null) {
      logger.error("Invalid axiom type: {}", p.getValueAsString());
      throw new IOException("Invalid axiom type: " + p.getValueAsString());
    }
    return axiomType;
  }
}
