/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import org.semanticweb.owlapi.model.AxiomType;

public class AxiomTypeSerializer extends StdSerializer<AxiomType> {

  public AxiomTypeSerializer() {
    this(null);
  }

  public AxiomTypeSerializer(Class<AxiomType> axiomTypeClass) {
    super(axiomTypeClass);
  }

  @Override
  public void serialize(AxiomType value, JsonGenerator gen, SerializerProvider provider)
      throws IOException {
    gen.writeString(value.getName());
  }
}
