/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.Optional;
import org.semanticweb.owlapi.model.EntityType;

public class EntityTypeDeserializer extends StdDeserializer<EntityType> {

  public EntityTypeDeserializer() {
    this(null);
  }

  public EntityTypeDeserializer(Class<EntityType> entityTypeClass) {
    super(entityTypeClass);
  }

  @Override
  public EntityType deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException {
    String value = p.getValueAsString();
    Optional<EntityType<?>> optEntityType = EntityType.values().stream()
        .filter(type -> type.toString().equals(value)).findAny();
    return optEntityType.orElseThrow(
        () -> new JsonProcessingException("Invalid entity type: " + value,
            p.getCurrentLocation()) {
        });
  }
}
