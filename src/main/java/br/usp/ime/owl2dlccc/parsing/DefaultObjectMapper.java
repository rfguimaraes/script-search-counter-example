/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.EntityType;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import br.usp.ime.owl2dlccc.model.ActionBlock;
import br.usp.ime.owl2dlccc.model.ActionBlockImpl;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.AxiomRequirementImpl;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import br.usp.ime.owl2dlccc.model.AxiomSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CatalystBlock;
import br.usp.ime.owl2dlccc.model.CatalystBlockImpl;
import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.model.ChangeSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.CompositeTermImpl;
import br.usp.ime.owl2dlccc.model.HeaderBlock;
import br.usp.ime.owl2dlccc.model.HeaderBlockImpl;
import br.usp.ime.owl2dlccc.model.IntegrityConstraints;
import br.usp.ime.owl2dlccc.model.IntegrityConstraintsImpl;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import br.usp.ime.owl2dlccc.model.PrimitiveTermImpl;
import br.usp.ime.owl2dlccc.model.TermsBlock;
import br.usp.ime.owl2dlccc.model.TermsBlockImpl;

public class DefaultObjectMapper extends ObjectMapper {

  private static DefaultObjectMapper defaultObjectMapper = null;

  public static DefaultObjectMapper getInstance() {
    if (defaultObjectMapper == null) {
      defaultObjectMapper = new DefaultObjectMapper();
    }
    return defaultObjectMapper;
  }

  private DefaultObjectMapper() {
    SimpleAbstractTypeResolver typeResolver = new SimpleAbstractTypeResolver();
    typeResolver.addMapping(ChangeSpecification.class, ChangeSpecificationImpl.class);
    typeResolver.addMapping(HeaderBlock.class, HeaderBlockImpl.class);
    typeResolver.addMapping(TermsBlock.class, TermsBlockImpl.class);
    typeResolver
        .addMapping(PrimitiveTerm.class, PrimitiveTermImpl.class);
    typeResolver
        .addMapping(CompositeTerm.class, CompositeTermImpl.class);
    typeResolver.addMapping(AxiomRequirement.class, AxiomRequirementImpl.class);
    typeResolver.addMapping(AxiomSpecification.class, AxiomSpecificationImpl.class);
    typeResolver.addMapping(ActionBlock.class, ActionBlockImpl.class);
    typeResolver.addMapping(CatalystBlock.class, CatalystBlockImpl.class);
    typeResolver.addMapping(IntegrityConstraints.class, IntegrityConstraintsImpl.class);

    SimpleModule typeModule = new SimpleModule("Test Type Mapping", Version.unknownVersion());
    typeModule.setAbstractTypes(typeResolver);
    typeModule.addSerializer(AxiomType.class, new AxiomTypeSerializer());
    typeModule.addDeserializer(AxiomType.class, new AxiomTypeDeserializer());
    typeModule.addSerializer(EntityType.class, new EntityTypeSerializer());
    typeModule.addDeserializer(EntityType.class, new EntityTypeDeserializer());

    this.registerModule(new Jdk8Module());
    this.registerModule(typeModule);
    this.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
    this.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true);
  }
}
