/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

@Disabled
public class FullMatchFactoryTest {

  private static final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  public static Stream<OWLOntology> ontologyProvider() {
    return Stream.of(loadOntology("/home/ricardo/Documentos/Academic/Ontologies/koalaS.owl"));
  }

  private static OWLOntology loadOntology(String path) {
    OWLOntology result = null;
    try {
      result = manager.loadOntologyFromOntologyDocument(Files.newInputStream(Paths.get(path)));
    } catch (OWLOntologyCreationException | IOException e) {
      e.printStackTrace();
    }
    return result;
  }

  @ParameterizedTest
  @MethodSource("ontologyProvider")
  void printNestedClassExpressions(OWLOntology ontology) {
    ontology.logicalAxioms().forEach(axiom -> {
      System.out.println("============");
      System.out.println(axiom);
      System.out.println("---------");
      axiom.nestedClassExpressions().forEach(
          System.out::println
      );
    });
  }
}
